﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="edit3.aspx.cs" Inherits="Carlsberg_new.edit3" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <style type="text/css">
            .scrollable tbody { display: block; height: 100px; width: 150px; overflow: auto; }
        </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

    <asp:Panel ID="Panel1" runat="server" BorderWidth="1">

    <div>
     <input type="button" value="Назад" onclick="history.back()">
    <table style="width: 100%;" border="1">
    <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444" 
                    style="font-weight: bold">
                    <span class="fontBlackBold FontColorStyle1">
                    <span class="style2" 
                        style="font-size: medium; color: #FFFFFF;"><strong>
                    Заявка</strong></span><strong><span class="style3" 
                        style="font-size: medium; color: #FFFFFF;">
                    </span> </strong></span>
                </td>
        </tr>
        <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label1" runat="server" Text="Тема:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox1" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>

          <tr>       
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label3" runat="server" Text="Название торговой точки:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox3" runat="server" Width="255px" ValidationGroup="v1" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="v1"></asp:RequiredFieldValidator>
            </td>                 
        </tr>

         <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label6" runat="server" Text="Адрес торговой точки:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox6" runat="server" Height="24px" Width="255px" 
                    ValidationGroup="v1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TextBox6" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="v1"></asp:RequiredFieldValidator>
            </td> 
         </tr>

         <tr>       
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label2" runat="server" Text="Описание:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Height="172px" 
                    Width="382px" ReadOnly="True" ></asp:TextBox>
            </td>                 
        </tr>      
        
        <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444" 
                    style="font-weight: bold">
                    <span class="fontBlackBold FontColorStyle1">
                    <span class="style2" 
                        style="font-size: medium; color: #FFFFFF;"><strong>
                    Сведения об инициаторе заявки</strong></span><strong><span class="style3">
                    </span> </strong></span>
                </td>
        </tr>
        <tr>       
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label4" runat="server" Text="ФИО:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox4" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td>                 
        </tr> 
        
        <tr>       
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label111" runat="server" Text="Телефон:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox111" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td>                 
        </tr> 

        <tr>       
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label222" runat="server" Text="Email:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox222" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td>                 
        </tr> 
        <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444">
                    <span class="fontBlackBold FontColorStyle1">
                    <span class="style2" 
                        style="font-size: medium; color: #FFFFFF;"><strong>
                    Информация по заявке</strong></span><strong><span class="style3">
                    </span> </strong></span>
                </td>
        </tr>          

        <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label8" runat="server" Text="Статус:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox8" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>

          <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label10" runat="server" Text="Дата подачи заявки:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox10" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>
         <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label5" runat="server" Text="ФИО ответственнего:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="FIO" 
                    DataValueField="s_id">
                </asp:DropDownList>
            </td> 
         </tr>
                           

          <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label7" runat="server" Text="Код СРМ:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox7" runat="server" Width="255px" ValidationGroup="v1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TextBox7" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="v1"></asp:RequiredFieldValidator>
            <asp:UpdatePanel ID="UpdatePanel111" runat="server" UpdateMode="Conditional">
             <ContentTemplate>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" 
                    ControlToValidate="TextBox7" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="v111"></asp:RequiredFieldValidator>
                    <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="Показать историю заявок" 
                    ValidationGroup="v111" />                 

                 <asp:GridView ID="G11" runat="server" AllowPaging="True" PageSize="20" 
                    CellPadding="4" GridLines="None" OnPageIndexChanging = "GridView1PageIndexChanging"
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                           <asp:TemplateField HeaderText = "&#1058;&#1077;&#1084;&#1072;" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton2"                                     
                                    Text='<%# Eval("theme") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>                       

                 <br />
             </ContentTemplate>
            </asp:UpdatePanel>  

            </td> 
         </tr>

          <tr>
            <td style="width: 194px" align="right">
                <b>
                <asp:Label ID="Label9" runat="server" Text="Комментарий:"></asp:Label>
                </b>
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBox9" runat="server" Width="380px" TextMode ="MultiLine" 
                    Height="105px"></asp:TextBox>
            </td> 
         </tr>
    </table>
        

        <asp:Panel ID="Panel221" runat="server" BorderWidth=1>
        <div>
        <asp:Label ID="Label11" runat="server" Text="Фото до:"></asp:Label>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>          
                    <table style="height:300px; width:500px">
                          <tr style="height:200px; width:500px">
                            <td>
                              <asp:Image ID="Image1" runat="server" ImageUrl="~/null.jpg" style="max-height:400px; max-width:500px;"/>
                            </td>
                          </tr>
                          <tr style="height:100px; width:500px">
                            <td>
                              <asp:DataList ID="DataList1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" >
                              <ItemTemplate>
                                <asp:ImageButton ID="imgBtn" runat="server" OnClick="imgBtn_Click11" ImageUrl='<%# "/Photos/" + Eval("Url") %>' Width="100px" Height="100px"  CommandArgument='<%# Container.ItemIndex %>' />
                              </ItemTemplate>
                              <SelectedItemStyle BorderColor="Red" BorderWidth="1px" />
                              </asp:DataList>
                            </td>
                          </tr> 
                     </table>             
            
           </ContentTemplate>      
        </asp:UpdatePanel> 
       </div>
    </asp:Panel>

    <br/>
    <asp:Panel ID="Panel2" runat="server" BorderWidth="1">
        <div>
            <br/>
            <asp:Button ID="Button1" runat="server" Text="Отправить" Font-Bold="True" 
                ForeColor="Black" onclick="Button1_Click" ValidationGroup="v1" />
        </div>
    </asp:Panel>
    </div>
    </asp:Panel>
    <asp:Label ID="Label113" runat="server" Text="Label"></asp:Label>
        <br />
     <input type="button" value="Назад" onclick="history.back()">
</asp:Content>
