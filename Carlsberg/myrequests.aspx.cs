﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Text.RegularExpressions;

namespace Carlsberg_new
{
    public partial class myrequests : System.Web.UI.Page
    {
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        string email = "yourlogin@example.com";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                TextBox1.Text = email;
            }
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            email = filter(TextBox1.Text);
            GridView1.DataSource = dta.GetDataByWorkerEmail(email);
            GridView1.DataBind();

            int rows = GridView1.Rows.Count;
            for (int i = 0; i < rows; i++)
            {
                if (GridView1.Rows[i].Cells[5].Text == "3") GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                if (GridView1.Rows[i].Cells[5].Text == "1" || GridView1.Rows[i].Cells[5].Text == "2") GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                if (GridView1.Rows[i].Cells[5].Text == "0") GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
            }
        }

        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/detail.aspx");
        }

        protected void GridView1PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = dta.GetDataByWorkerEmail(email);
            GridView1.DataBind();

            int rows = GridView1.Rows.Count;
            for (int i = 0; i < rows; i++)
            {
                if (GridView1.Rows[i].Cells[5].Text == "3") GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                if (GridView1.Rows[i].Cells[5].Text == "1" || GridView1.Rows[i].Cells[5].Text == "2") GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                if (GridView1.Rows[i].Cells[5].Text == "0") GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
            }
        }

        string filter(string txt)
        {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }
    }
}