﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="myrequests.aspx.cs" Inherits="Carlsberg_new.myrequests" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:Label ID="Label1" runat="server" Text="Введите email:"></asp:Label>
    <asp:TextBox ID="TextBox1" runat="server" Width="205px"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">back</asp:HyperLink>
    <br />
    <asp:button runat="server" text="Поиск" onclick="Unnamed1_Click" />
    
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30" 
                    CellPadding="4" GridLines="None" OnPageIndexChanging = "GridView1PageIndexChanging"
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                           <asp:TemplateField HeaderText = "&#1058;&#1077;&#1084;&#1072;" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton2"                                     
                                    Text='<%# Eval("theme") %>'              
                                    OnClick="LinkButton_Click1" 
                                    CommandArgument=<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="workerFIO" HeaderText="ФИО подавшего заявку" 
                                SortExpression="workerFIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="FIO" HeaderText="Ответственный" 
                                SortExpression="FIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="address" HeaderText="&#1040;&#1076;&#1088;&#1077;&#1089;" SortExpression="address" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="date" HeaderText="&#1044;&#1072;&#1090;&#1072;" SortExpression="date" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="p_type" HeaderText="&#1057;&#1090;&#1072;&#1090;&#1091;&#1089;" SortExpression="p_type" 
                                ItemStyle-Width="70px">
                <ItemStyle Width="70px"></ItemStyle>
                            </asp:BoundField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>      
                     
</asp:Content>

