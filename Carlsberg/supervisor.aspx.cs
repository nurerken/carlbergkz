﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Web.Security;
using System.Net.Mail;
using System.Net;

namespace Carlsberg_new
{
    public partial class supervisor : System.Web.UI.Page
    {
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();

        static string date1 = System.DateTime.Today.ToString("yyyy-MM-1");
        static string date2 = System.DateTime.Today.ToString("yyyy-MM-dd");
        static bool b = false;
        string userID = "";
        string userType = "";
        static bool b2 = false;
                              
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(userID).Rows[0]["type"].ToString();
                
                if (userType.Equals("1"))
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        if (!IsPostBack)
                        {
                            UpdatePanel UpdatePanel1 = (UpdatePanel)LoginView1.FindControl("UpdatePanel1");
                            DropDownList DropDownList1 = (DropDownList)UpdatePanel1.FindControl("DropDownList1");
                            DropDownList1.Items.Insert(0, new ListItem("всем", "all"));
                            DropDownList1.Items.Insert(1, new ListItem("закрытым", "closed"));
                            DropDownList1.Items.Insert(2, new ListItem("открытым", "unclosed"));                            
                        }
                    }
                    if (!IsPostBack)
                    {
                        if (User.Identity.IsAuthenticated)
                        {
                            Label Label1111 = (Label)LoginView1.FindControl("Label1111");
                            Label Label5555 = (Label)LoginView1.FindControl("Label5555");

                            Label1111.Text = date1;
                            Label5555.Text = date2;

                            GridView GridView1 = (GridView)LoginView1.FindControl("GridView1");
                            GridView GridView2 = (GridView)LoginView1.FindControl("GridView2");

                            GridView2.DataSource = dta.GetDataFor_Supervisor(userID);
                            GridView2.DataBind();

                            GridView1.DataSource = dta.GetDataForSupervisorDefault(userID);
                            GridView1.DataBind();
                            
                            int rows = GridView1.Rows.Count;
                            for (int i = 0; i < rows; i++)
                            {
                                if (GridView1.Rows[i].Cells[5].Text == "3" ) GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                                if (GridView1.Rows[i].Cells[5].Text == "1" || GridView1.Rows[i].Cells[5].Text == "2") GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                                if (GridView1.Rows[i].Cells[5].Text == "0") GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                                GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
                            }

                            rows = GridView2.Rows.Count;

                            for (int i = 0; i < rows; i++)
                            {
                                if (GridView2.Rows[i].Cells[5].Text == "3" ) GridView2.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                                if (GridView2.Rows[i].Cells[5].Text == "1" || GridView2.Rows[i].Cells[5].Text == "2") GridView2.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                                if (GridView2.Rows[i].Cells[5].Text == "0") GridView2.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                                GridView2.Rows[i].Cells[4].Text = (GridView2.Rows[i].Cells[4].Text).Substring(0, 10);
                            }
                        }
                    }
                }
                else Response.Redirect("default.aspx");
            }
            else Response.Redirect("default.aspx");
        }

        protected void GridViewPageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                GridView GridView1 = (GridView)LoginView1.FindControl("GridView1");
                GridView GridView2 = (GridView)LoginView1.FindControl("GridView2");

                GridView2.PageIndex = e.NewPageIndex;
                GridView2.DataSource = dta.GetDataFor_Supervisor(userID);
                GridView2.DataBind();

                int rows = GridView2.Rows.Count;

                for (int i = 0; i < rows; i++)
                {
                    if (GridView2.Rows[i].Cells[5].Text == "3") GridView2.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                    if (GridView2.Rows[i].Cells[5].Text == "1" || GridView2.Rows[i].Cells[5].Text == "2") GridView2.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                    if (GridView2.Rows[i].Cells[5].Text == "0") GridView2.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                    GridView2.Rows[i].Cells[4].Text = (GridView2.Rows[i].Cells[4].Text).Substring(0, 10);
                }
            }
        }

        protected void GridView1PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                GridView GridView1 = (GridView)LoginView1.FindControl("GridView1");
                GridView GridView2 = (GridView)LoginView1.FindControl("GridView2");

                GridView1.PageIndex = e.NewPageIndex;
                if (!b)
                    GridView1.DataSource = dta.GetDataForSupervisorDefault(userID);
                else
                    GridView1.DataSource = dta.GetDataForSupervisorByDate(userID,Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                GridView1.DataBind();

                int rows = GridView1.Rows.Count;
                for (int i = 0; i < rows; i++)
                {
                    if (GridView1.Rows[i].Cells[5].Text == "3") GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                    if (GridView1.Rows[i].Cells[5].Text == "1" || GridView1.Rows[i].Cells[5].Text == "2") GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                    if (GridView1.Rows[i].Cells[5].Text == "0") GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                    GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
                }

                System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
                sb22.Append("<script language=\"javascript\">");
                sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
                sb22.Append("document.getElementById(\"all\").className=\"active\";");
                sb22.Append("document.getElementById(\"new1\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"all1\").className=\"tab-pane active\";");
                sb22.Append("</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());
            }
        }

        protected void LinkButton_Click2(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/edit2.aspx");
        }

        protected void LinkButton_Click(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/details.aspx");
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Calendar Calendar1 = (Calendar)LoginView1.FindControl("Calendar1");
                Calendar1.Attributes.Add("onClick", "hide111();");
                Label Label1111 = (Label)LoginView1.FindControl("Label1111");
                date1 = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
                Label1111.Text = date1;
                b2 = true;
            }
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            {
                Calendar Calendar2 = (Calendar)LoginView1.FindControl("Calendar2");
                Label Label5555 = (Label)LoginView1.FindControl("Label5555");
                date2 = Calendar2.SelectedDate.ToString("yyyy-MM-dd");
                Label5555.Text = date2;
                b2 = true;
            }
        }

        protected void btn1_click(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
                sb22.Append("<script language=\"javascript\">");
                sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
                sb22.Append("document.getElementById(\"all\").className=\"active\";");
                sb22.Append("document.getElementById(\"new1\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"all1\").className=\"tab-pane active\";");
                sb22.Append("</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());

                b = true;

                GridView GridView1 = (GridView)LoginView1.FindControl("GridView1");
                GridView GridView2 = (GridView)LoginView1.FindControl("GridView2");

                GridView1.DataSource = dta.GetDataForSupervisorByDate(userID,Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                GridView1.DataBind();

                int rows = GridView1.Rows.Count;

                for (int i = 0; i < rows; i++)
                {
                    if (GridView1.Rows[i].Cells[5].Text == "3") GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                    if (GridView1.Rows[i].Cells[5].Text == "1" || GridView1.Rows[i].Cells[5].Text == "2") GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                    if (GridView1.Rows[i].Cells[5].Text == "0") GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                    GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
                }
            }

        }

        protected void printClosed(object sender, EventArgs e)
        {
            UpdatePanel UpdatePanel1 = (UpdatePanel)LoginView1.FindControl("UpdatePanel1");
            DropDownList DropDownList1 = (DropDownList)UpdatePanel1.FindControl("DropDownList1");
            
            String userFIO = sta.GetDataBySID(User.Identity.Name).Rows[0]["FIO"].ToString();
            string reportType = "3";
            string selectedValue = DropDownList1.SelectedValue;
            if (selectedValue == "closed") reportType = "0";
            if (selectedValue == "unclosed") reportType = "1";
            if (selectedValue == "all") reportType = "2";

            string date11 = FirstDayOfMonthFromDateTime(System.DateTime.Today).ToString().Substring(0,10);
            string date22 = (System.DateTime.Today).ToString().Substring(0,10);

            if(b2)
                Response.Redirect("print.aspx?user=" + userFIO + "&type=" + reportType + "&date1=" + date1 + "&date2=" + date2);
            else
                Response.Redirect("print.aspx?user=" + userFIO + "&type=" + reportType + "&date1=" + date11 + "&date2=" + date22);
        }

        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }        
    }
}
