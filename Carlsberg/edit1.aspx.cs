﻿using System.Configuration;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Security;

namespace Carlsberg_new
{
    public partial class edit1 : System.Web.UI.Page
    {
        private String p_id = "";
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        string userType = "";
        string userID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(userID).Rows[0]["type"].ToString();
                if (userType.Equals("2"))
                {
                    if (Session["p_id"] != null)
                    {
                        p_id = Session["p_id"].ToString();
                        Label113.Visible = false;

                        if (!Page.IsPostBack)
                        {
                            DataTable table = dta.GetSingleDataByID(Int32.Parse(p_id));
                            TextBox1.Text = table.Rows[0]["theme"].ToString();
                            TextBox2.Text = table.Rows[0]["description"].ToString();
                            TextBox3.Text = table.Rows[0]["nazvanie"].ToString();
                            TextBox4.Text = table.Rows[0]["workerFIO"].ToString();
                            TextBox111.Text = table.Rows[0]["workerPhone"].ToString();
                            TextBox222.Text = table.Rows[0]["workerEmail"].ToString();
                            TextBox5.Text = table.Rows[0]["FIO"].ToString();
                            TextBox333.Text = table.Rows[0]["phone"].ToString();
                            //TextBox444.Text = table.Rows[0]["email"].ToString();
                            HyperLink1.Text = table.Rows[0]["email"].ToString();
                            HyperLink1.NavigateUrl = "mailto:" + table.Rows[0]["email"].ToString();

                            TextBox6.Text = table.Rows[0]["address"].ToString();
                            TextBox7.Text = table.Rows[0]["codeCRM"].ToString();
                        
                            if (table.Rows[0]["p_type"].ToString().Equals("3"))
                                TextBox8.Text = "закрытая";
                            if (table.Rows[0]["p_type"].ToString().Equals("1") || table.Rows[0]["p_type"].ToString().Equals("2"))
                            {
                                TextBox8.Text = "открытая";
                            }
                            if (table.Rows[0]["p_type"].ToString().Equals("0"))
                                TextBox8.Text = "неназначенная";

                            TextBox10.Text = table.Rows[0]["date"].ToString().Substring(0, 10);
                            TextBox9.Text = table.Rows[0]["comments"].ToString();

                            String photos_before = "";
                            String photos_after = "";

                            photos_before = table.Rows[0]["photo_before"].ToString();
                            photos_after = table.Rows[0]["photo_after"].ToString();

                            String[] photos = photos_before.Split('|');
                            String[] photos2 = photos_after.Split('|');

                            FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photos[0]);
                            if (TheFile.Exists)
                            {
                                Image1.ImageUrl = "Photos/" + photos[0];
                               /* Image tempImage = new Image();//original image
                                tempImage.ImageUrl = "Photos/" + photos[0];

                                Image tempImage2 = new Image();//maxsizes
                                tempImage2.Width = 1000;
                                tempImage2.Height = 1000;

                                if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
                                else Image1.Width = tempImage2.Width;

                                if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
                                else Image1.Height = tempImage2.Height;*/
                            }
                            else Image1.ImageUrl = "null.jpg";

                            TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photos2[0]);
                            if (TheFile.Exists)
                            {
                                Image2.ImageUrl = "Photos/" + photos2[0];
                                /*Image tempImage = new Image();//original image
                                tempImage.ImageUrl = "Photos/" + photos[0];

                                Image tempImage2 = new Image();//maxsizes
                                tempImage2.Width = 1000;
                                tempImage2.Height = 1000;

                                if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image2.Width = tempImage.Width;
                                else Image2.Width = tempImage2.Width;

                                if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image2.Height = tempImage.Height;
                                else Image2.Height = tempImage2.Height;*/
                            }
                            else Image2.ImageUrl = "null.jpg";

                            DataList1.DataSource = BindGrid(photos);
                            DataList1.DataBind();

                            DataList2.DataSource = BindGrid(photos2);
                            DataList2.DataBind();
                        }
                    }
                    else Response.Redirect("default.aspx");
                }
                else Response.Redirect("default.aspx");
            }
            else Response.Redirect("default.aspx");
        }

        protected DataTable BindGrid(String[] photos)
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("/Photos/"));
            FileInfo[] fi2 = di.GetFiles();
            List<FileInfo> fi = new List<FileInfo>();

            //Filter images
            for (int i = 0; i < fi2.Count(); i++)
            {
                for (int j = 0; j < photos.Count() - 1; j++)
                {
                    if (fi2[i].ToString().Equals(photos[j]))
                    {
                        fi.Add(fi2[i]);
                    }
                }
            }

            // Save all paths to the DataTable as the data source.
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Url", typeof(System.String));
            dt.Columns.Add(dc);

            for (int i = 0; i < fi.Count(); i++)
            {
                DataRow dro = dt.NewRow();
                dro[0] = fi[i].Name;
                dt.Rows.Add(dro);
            }
            return dt;
        }

        // Handle the thumbnail image selecting event.
        protected void imgBtn_Click(object sender, EventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            Image1.ImageUrl = ib.ImageUrl;

            Image tempImage = new Image();
            tempImage.ImageUrl = ib.ImageUrl;

            Image tempImage2 = new Image();//maxsizes
            tempImage2.Width = 1000;
            tempImage2.Height = 1000;

            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
            else Image1.Width = tempImage2.Width;

            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
            else Image1.Height = tempImage2.Height;

            DataList1.SelectedIndex = Convert.ToInt32(ib.CommandArgument);
        }

        protected void imgBtn_Click2(object sender, EventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            Image2.ImageUrl = ib.ImageUrl;

            Image tempImage = new Image();//original image
            tempImage.ImageUrl = ib.ImageUrl;

            Image tempImage2 = new Image();//maxsizes
            tempImage2.Width = 1000;
            tempImage2.Height = 1000;

            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image2.Width = tempImage.Width;
            else Image2.Width = tempImage2.Width;

            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image2.Height = tempImage.Height;
            else Image2.Height = tempImage2.Height;

            DataList2.SelectedIndex = Convert.ToInt32(ib.CommandArgument);
        }

        protected void Button1_Click(object sender, EventArgs e)//accept
        {
            String comment = TextBox9.Text;
            dta.UpdateQuery_acceptORdeny("3", comment, Int32.Parse(p_id));

            //send email to работник 
           string wEmail = dta.GetSingleDataByID(Int32.Parse(p_id)).Rows[0]["WorkerEmail"].ToString();
           /* sendEmail(wEmail, "Заявка принята", "В системе Beer Request была принята заявка. Тема заявки: " + TextBox1.Text + ", Название ТТ: " + TextBox3.Text + ", Адрес ТТ:" + TextBox6.Text + " \n " + "Описание:" + " \n " + TextBox2.Text);*/

            //-------------- Посыл майло ------------------------------------------------
            MailAlert Message = new MailAlert();

            //Инициатору
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[8];
            Alert[0].ParamName = "@TTName@";    Alert[0].ParamValue = TextBox3.Text;
            Alert[1].ParamName = "@TTAddress@"; Alert[1].ParamValue = TextBox6.Text;
            Alert[2].ParamName = "@Subject@";   Alert[2].ParamValue = TextBox1.Text;
            Alert[3].ParamName = "@Desc@";      Alert[3].ParamValue = TextBox2.Text;
            Alert[4].ParamName = "@S_FIO@";         Alert[4].ParamValue = TextBox6.Text;
            Alert[5].ParamName = "@S_PhoneNumber@"; Alert[5].ParamValue = TextBox333.Text;
            Alert[6].ParamName = "@S_Email@";       Alert[6].ParamValue = HyperLink1.Text;
            Alert[7].ParamName = "@K_Desc@";        Alert[7].ParamValue = TextBox9.Text;
            Message.SendAlert("close", wEmail, "Заявка выполнена", Alert).ToString();
            //----------------------------------------------------------------------------

            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Заявка одобрена');");
            Response.Write("document.location.href='admin.aspx';");
            //Response.Write("window.close();");
            Response.Write("</script>");

        }

        protected void Button2_Click(object sender, EventArgs e)//deny
        {
            String comment = TextBox9.Text;
            
            dta.UpdateQuery_acceptORdeny("1", comment, Int32.Parse(p_id));
            
            string data = "";
            try
            {
                data += dta.GetSingleDataByID(Int32.Parse(p_id)).Rows[0]["photo_after"].ToString();
            }
            catch (Exception ex) { Response.Write(ex.Message); }
            
            String[] photo = data.Split('|');
            for (int i = 0; i < photo.Length - 1; i++)//delete photos
            {
                try
                {
                    FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photo[i]);
                    if (TheFile.Exists)
                    {
                        File.Delete(Server.MapPath("Photos") + "\\" + photo[i]);
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }

            //send email to supervisor
            string sEmail = dta.GetSingleDataByID(Int32.Parse(p_id)).Rows[0]["email"].ToString();
           // sendEmail(sEmail, "Заявка не принята", "тема: " + TextBox1.Text + ", Название ТТ: " + TextBox3.Text + ", Адрес ТТ:" + TextBox6.Text);
            //-------------- Посыл майло ------------------------------------------------
            MailAlert Message = new MailAlert();

            //Инициатору
            MailAlert.AlertList[] Alert = new MailAlert.AlertList[5];
            Alert[0].ParamName = "@TTName@";    Alert[0].ParamValue = TextBox3.Text;
            Alert[1].ParamName = "@TTAddress@"; Alert[1].ParamValue = TextBox6.Text;
            Alert[2].ParamName = "@Subject@";   Alert[2].ParamValue = TextBox1.Text;
            Alert[3].ParamName = "@Desc@";      Alert[3].ParamValue = TextBox2.Text;
            Alert[4].ParamName = "@K_Desc@";    Alert[4].ParamValue = TextBox9.Text;
            Message.SendAlert("notclose_s", sEmail, "Заявка не выполнена", Alert).ToString();
            //----------------------------------------------------------------------------

            
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Заявка отклонена');");
            Response.Write("document.location.href='admin.aspx';");
            //Response.Write("window.close();");
            Response.Write("</script>");
        }

        void sendEmail(string mailto, string subject, string body)
        {
            try
            {
                mydataTableAdapter mta = new mydataTableAdapter();
                string email = mta.GetData().Rows[0]["email"].ToString();
                string password = mta.GetData().Rows[0]["password"].ToString();
                MailMessage objMail = new MailMessage(email, mailto, subject, body);
                NetworkCredential objNC = new NetworkCredential(email, password);
                SmtpClient objsmtp = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTP"), Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort")));
                objsmtp.EnableSsl = true;
                objsmtp.Credentials = objNC;
                objsmtp.Send(objMail);
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

    }
}