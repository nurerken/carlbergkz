﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="createUser.aspx.cs" Inherits="Carlsberg_new.createUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    

<style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
        .modalPopup
        {
            width: 250px;
            height: 180px;
            background-color: #076DAB;
            color: #FFFFFF;
            border-color: #000000;
            border-width: 1px;
            border-style: solid;
            text-align: center;
            cursor: move;
            font-size: medium;
        }
        .style2
        {            font-size: medium;
        }
        .style3
        {
            font-size: large;
        }
    .reqFormsubHead
    {
        color: #FFFFFF;
    }
    </style>
     <asp:scriptmanager ID="Scriptmanager1" runat="server">
    </asp:scriptmanager>
    <input type="button" value="Назад" onclick="history.back()">
    <asp:Panel ID="Panel11" runat="server" BorderWidth="1">
    <table border="0" cellpadding="1" cellspacing="0" style="border-collapse: collapse;
                            width: 100%">
            <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444">
                    <span class="fontBlackBold FontColorStyle1"><span class="style2"></span><strong><span class="style3">
                    </span> </strong></span>
                    <asp:label ID="label1" runat="server" text="new user" Font-Bold="True" 
                        CssClass="style2"></asp:label>
                </td>
            </tr>
            <tr>
            <td align="right" style="width: 150px">
                <asp:label ID="lb1" runat="server" text="UserID" style="font-weight: 700"></asp:label>
            </td>
            <td>
                &nbsp;
                <asp:textbox ID="tb1" runat="server" Width="183px"></asp:textbox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="tb1" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            </tr>
            <tr>
            <td align="right" style="width: 150px">
                <asp:label ID="lb2" runat="server" text="ФИО" style="font-weight: 700"></asp:label>
            </td>
            <td>
                &nbsp;
                <asp:textbox ID="tb2" runat="server" Width="449px"></asp:textbox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="tb2" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            </tr>
            <tr>
            <td align="right" style="width: 150px">
                <asp:label ID="lb3" runat="server" text="Email" style="font-weight: 700"></asp:label>
            </td>
            <td>
                &nbsp;
                <asp:textbox ID="tb3" runat="server" Width="291px"></asp:textbox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="tb3" ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="tb3" ErrorMessage="неправильный email" 
                    ValidationExpression="^(?(&quot;&quot;)(&quot;&quot;.+?&quot;&quot;@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&amp;'\*\+/=\?\^`\{\}\|~\w])*)(?&lt;=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"></asp:RegularExpressionValidator>
            </td>
            </tr>
            <tr>
                <td align="right" style="width: 150px">
                    <asp:label ID="lb4" runat="server" text="Телефон" style="font-weight: 700"></asp:label>
                </td>
                <td>
                    &nbsp;
                    <asp:textbox ID="tb4" runat="server" Width="168px"></asp:textbox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="tb4" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td align="right" style="width: 150px">
                    <asp:label ID="lb5" runat="server" text="Пароль" style="font-weight: 700"></asp:label>
                </td>
                <td>
                    &nbsp;
                    <asp:textbox ID="tb5" runat="server" type="password" Width="167px"></asp:textbox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="tb5" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td align="right" style="width: 150px">
                    <asp:label ID="lb6" runat="server" text="     Подтвердите пароль" 
                        style="font-weight: 700"></asp:label>
                </td>
                <td>
                    &nbsp;
                    <asp:textbox ID="tb6" runat="server" type="password" Width="168px"></asp:textbox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="tb6" ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="tb6" ControlToValidate="tb5" 
                    ErrorMessage="Пароли не совпадают"></asp:CompareValidator>
                </td>
            </tr>
    </table>
    
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:button ID="button1" runat="server" text="Создать" onClientClick="return dsfsdf();"
        onclick="button1_Click" />

</asp:Panel>   

        <script type="text/javascript">
            function dsfsdf() {
                //3,6,2,7
                var t1 = document.getElementById("ContentPlaceHolder1_tb1").value; //userID
                var t2 = document.getElementById("ContentPlaceHolder1_tb2").value; //FIO
                var t3 = document.getElementById("ContentPlaceHolder1_tb3").value; //email
                var t4 = document.getElementById("ContentPlaceHolder1_tb4").value; //phone
                var t5 = document.getElementById("ContentPlaceHolder1_tb5").value; //password
                var t6 = document.getElementById("ContentPlaceHolder1_tb6").value; //confirm pas
                
                if (t1.toString() == "") {
                    alert("пожалуйста, заполните поле название UserID");
                    return false;
                }
                if (t2.toString() == "") {
                    alert("пожалуйста, заполните поле ФИО");
                    return false;
                }
                if (t3.toString() == "") {
                    alert("пожалуйста, заполните поле email");
                    return false;
                }
                if (t4.toString() == "") {
                    alert("пожалуйста, заполните поле номер телефона");
                    return false;
                }
                if (t5.toString() == "") {
                    alert("пожалуйста, заполните поле пароль");
                    return false;
                } 
                if (t6.toString() == "") {
                    alert("пожалуйста, заполните поле подтверждение пороля");
                    return false;
                }
                if (t6.toString() != t5.toString()) {
                    alert("пароли не совпадают");
                    return false;
                }

                return true;
            }
    </script>

    </asp:Content>
