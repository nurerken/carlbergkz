﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="Carlsberg_new.EditProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <input type="button" value="Назад" onclick="history.back()">
      <table>  
        <tr>
            <td>
                <asp:Label ID="lable2" runat="server" Text="ФИО"></asp:Label>
            </td>
            <td>
                <asp:TextBox id="tb1" runat="server" ValidationGroup="v1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="tb1" ErrorMessage="*" ValidationGroup="v1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            
            <td>
                <asp:label ID="lable3" runat="server" text="email"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb2" runat="server" ValidationGroup="v1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" 
                    ControlToValidate="tb2" ErrorMessage="*" ValidationGroup="v1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                    ControlToValidate="tb2" ErrorMessage="неправильный email" 
                    ValidationExpression="^(?(&quot;&quot;)(&quot;&quot;.+?&quot;&quot;@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&amp;'\*\+/=\?\^`\{\}\|~\w])*)(?&lt;=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$" 
                    ValidationGroup="v1"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:label ID="lable4" runat="server" text="телефон"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb3" runat="server" ValidationGroup="v1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="tb3" ErrorMessage="*" ValidationGroup="v1"></asp:RequiredFieldValidator>
            </td>
        </tr>
      </table>
    <asp:button id="button1" runat="server" text="Изменить" onclick="button1_Click" onclientclick = "return abc();"
          ValidationGroup="v1" />
          <script type="text/javascript">
                  function abc() {
                      
                      var t1 = document.getElementById("ContentPlaceHolder1_tb1").value; //FIO
                      var t2 = document.getElementById("ContentPlaceHolder1_tb2").value; //email
                      var t3 = document.getElementById("ContentPlaceHolder1_tb3").value; //phone
                      
                      if (t1.toString() == "") {
                          alert("пожалуйста, заполните поле название ФИО");
                          return false;
                      }
                      if (t2.toString() == "") {
                          alert("пожалуйста, заполните поле email");
                          return false;
                      }
                      if (t3.toString() == "") {
                          alert("пожалуйста, заполните поле номер телефона");
                          return false;
                      }                     
                      return true;
                  }
        </script>

    <br/>
    <hr/>
    <asp:Panel ID="Panel1" runat="server" >    
      <table>  
        <tr>
            <td>
                <asp:Label ID="lable22" runat="server" Text="Текущий пароль"></asp:Label>
            </td>
            <td>
                <asp:TextBox id="tb11" runat="server" ValidationGroup="v2" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                    ControlToValidate="tb11" ErrorMessage="*" ValidationGroup="v2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            
            <td>
                <asp:label ID="lable33" runat="server" text="Новый пароль"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb22" runat="server" ValidationGroup="v2" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                    ControlToValidate="tb22" ErrorMessage="*" ValidationGroup="v2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:label ID="lable44" runat="server" text="Подтвердить новый пароль"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb33" runat="server" ValidationGroup="v2" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" 
                    ControlToValidate="tb33" ErrorMessage="*" ValidationGroup="v2"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="tb22" ControlToValidate="tb33" 
                    ErrorMessage="Пароли не совпадают" ValidationGroup="v2"></asp:CompareValidator>
            </td>
        </tr>
      </table>
    <asp:button id="button11" runat="server" text="Изменить пароль" 
            onclick="button11_Click" ValidationGroup="v2" />
    </asp:Panel>
</asp:Content>
