﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;
using System.IO;

namespace Carlsberg_new
{
    public partial class browse : System.Web.UI.Page
    {
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        static string date1 = System.DateTime.Today.ToString("yyyy-MM-1");
        static string date2 = System.DateTime.Today.ToString("yyyy-MM-dd");
        static bool b1 = false;
        static bool b2 = false;
        string adminID="";
        static bool b = false;
        string userType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                adminID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(adminID).Rows[0]["type"].ToString();

                if (userType.Equals("4"))
                {
                    if (!IsPostBack)
                    {
                        if (User.Identity.IsAuthenticated)
                        {
                            UpdatePanel UpdatePanel1 = (UpdatePanel)LoginView1.FindControl("UpdatePanel1");
                            DropDownList DropDownList1 = (DropDownList)UpdatePanel1.FindControl("DropDownList1");
                            DropDownList1.Items.Insert(0, new ListItem("всем", "all"));
                            DropDownList1.Items.Insert(1, new ListItem("открытым и неназначенным", "openAndnenaz"));

                            Label LabelDo = (Label)LoginView1.FindControl("LabelDo");
                            Label LabelPosle = (Label)LoginView1.FindControl("LabelPosle");

                            LabelDo.Text = date1;
                            LabelPosle.Text = date2;
                       
                            GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                            
                            GridView3.DataSource = dta.GetDataFromFirstDay();
                            GridView3.DataBind();

                            int rows = GridView3.Rows.Count;

                            for (int i = 0; i < rows; i++)
                            {
                                if (GridView3.Rows[i].Cells[5].Text == "3") GridView3.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                                if (GridView3.Rows[i].Cells[5].Text == "1" || GridView3.Rows[i].Cells[5].Text == "2") GridView3.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                                if (GridView3.Rows[i].Cells[5].Text == "0") GridView3.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                                GridView3.Rows[i].Cells[4].Text = (GridView3.Rows[i].Cells[4].Text).Substring(0, 10);
                            }
                        }
                    }                   
                }
                else {
                    Response.Redirect("default.aspx");
                }
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

     
       protected void GridView3PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                GridView3.PageIndex = e.NewPageIndex;
                if (b)
                    GridView3.DataSource = dta.GetDataBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                else
                    GridView3.DataSource = dta.GetDataFromFirstDay();

                GridView3.DataBind();

                int rows = GridView3.Rows.Count;
                for (int i = 0; i < rows; i++)
                {
                    if (GridView3.Rows[i].Cells[5].Text == "3") GridView3.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                    if (GridView3.Rows[i].Cells[5].Text == "1" || GridView3.Rows[i].Cells[5].Text == "2") GridView3.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                    if (GridView3.Rows[i].Cells[5].Text == "0") GridView3.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                    GridView3.Rows[i].Cells[4].Text = (GridView3.Rows[i].Cells[4].Text).Substring(0, 10);
                }

                System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
                sb22.Append("<script language=\"javascript\">");
                sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
                sb22.Append("document.getElementById(\"ready\").className=\"passivedl\";");
                sb22.Append("document.getElementById(\"all\").className=\"active\";");
                sb22.Append("document.getElementById(\"inbox\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"outbox\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"newmess\").className=\"tab-pane active\";");
                sb22.Append("</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());
            }
        }

        protected void LinkButton_Click3(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/details.aspx");
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Calendar Calendar1 = (Calendar)LoginView1.FindControl("Calendar1");
                Label LabelDo = (Label)LoginView1.FindControl("LabelDo");
                date1 = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
                LabelDo.Text = date1;
                b1 = true;
            }
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Calendar Calendar2 = (Calendar)LoginView1.FindControl("Calendar2");
                Label LabelPosle = (Label)LoginView1.FindControl("LabelPosle");
                date2 = Calendar2.SelectedDate.ToString("yyyy-MM-dd");
                LabelPosle.Text = date2;
                b2 = true;
            }
        }

        protected void btn1_click(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {             
                GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                GridView3.DataSource = dta.GetDataBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                GridView3.DataBind();

                b = true;
                int rows = GridView3.Rows.Count;

                for (int i = 0; i < rows; i++)
                {
                    if (GridView3.Rows[i].Cells[5].Text == "3") GridView3.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                    if (GridView3.Rows[i].Cells[5].Text == "1" || GridView3.Rows[i].Cells[5].Text == "2") GridView3.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                    if (GridView3.Rows[i].Cells[5].Text == "0") GridView3.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                    GridView3.Rows[i].Cells[4].Text = (GridView3.Rows[i].Cells[4].Text).Substring(0, 10);
                }
            }
        }

        protected void printClosed(object sender, EventArgs e)
        {
            UpdatePanel UpdatePanel1 = (UpdatePanel)LoginView1.FindControl("UpdatePanel1");
            DropDownList DropDownList1 = (DropDownList)UpdatePanel1.FindControl("DropDownList1");

            string reportType = "0";
            string selectedValue = DropDownList1.SelectedValue;
            if (selectedValue == "all") reportType = "0";
            if (selectedValue == "openAndnenaz") reportType = "1";

            Response.Redirect("reports.aspx?date1=" + date1 + "&date2=" + date2 + "&reporttype=" + reportType);
        }
    }
}