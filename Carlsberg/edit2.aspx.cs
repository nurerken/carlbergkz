﻿using System.Configuration;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Collections;

namespace Carlsberg_new
{
    public partial class edit2 : System.Web.UI.Page
    {
        private String p_id = "";
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        string usertype = "";
        string userID = "";
                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                usertype = sta.GetDataBySID(userID).Rows[0]["type"].ToString();
                if (usertype.Equals("1"))
                {
                    if (Session["p_id"] != null)
                    {
                        p_id = Session["p_id"].ToString();
                        Label113.Visible = false;

                        if (!Page.IsPostBack)
                        {
                            DataTable table = dta.GetSingleDataByID(Int32.Parse(p_id));
                            TextBox1.Text = table.Rows[0]["theme"].ToString();
                            TextBox2.Text = table.Rows[0]["description"].ToString();
                            TextBox3.Text = table.Rows[0]["nazvanie"].ToString();
                            TextBox4.Text = table.Rows[0]["workerFIO"].ToString();
                            TextBox5.Text = table.Rows[0]["FIO"].ToString();
                            TextBox6.Text = table.Rows[0]["address"].ToString();
                            TextBox7.Text = table.Rows[0]["codeCRM"].ToString();
                            
                            if (table.Rows[0]["p_type"].ToString().Equals("3"))
                                TextBox8.Text = "закрытая";
                            if (table.Rows[0]["p_type"].ToString().Equals("1") || table.Rows[0]["p_type"].ToString().Equals("2"))
                            {
                                TextBox8.Text = "открытая";
                            }
                            if (table.Rows[0]["p_type"].ToString().Equals("0"))
                                TextBox8.Text = "неназначенная";

                            TextBox10.Text = table.Rows[0]["date"].ToString().Substring(0, 10);
                            TextBox9.Text = table.Rows[0]["comments"].ToString();

                            TextBox111.Text = table.Rows[0]["workerPhone"].ToString();
                            TextBox222.Text = table.Rows[0]["workerEmail"].ToString();
                            TextBox333.Text = table.Rows[0]["phone"].ToString();
                            //TextBox444.Text = table.Rows[0]["email"].ToString();
                            HyperLink1.Text = table.Rows[0]["email"].ToString();
                            HyperLink1.NavigateUrl = "mailto:" + table.Rows[0]["email"].ToString();

                            String photos_before = "";
                            String photos_after = "";

                            photos_before = table.Rows[0]["photo_before"].ToString();
                            photos_after = table.Rows[0]["photo_after"].ToString();

                            String[] photos = photos_before.Split('|');
                            String[] photos2 = photos_after.Split('|');

                            FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photos[0]);
                            if (TheFile.Exists)
                            {
                                Image1.ImageUrl = "Photos/" + photos[0];
                                Image tempImage = new Image();//original image
                                tempImage.ImageUrl = "Photos/" + photos[0];

                                Image tempImage2 = new Image();//maxsizes
                                tempImage2.Width = 1000;
                                tempImage2.Height = 1000;

                                if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
                                else Image1.Width = tempImage2.Width;

                                if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
                                else Image1.Height = tempImage2.Height;
                            }
                            else Image1.ImageUrl = "null.jpg";                            
                            
                            DataList1.DataSource = BindGrid(photos);
                            DataList1.DataBind();
                        }
                    }
                    else Response.Redirect("default.aspx");
                }
                else Response.Redirect("default.aspx");
            }
            else Response.Redirect("default.aspx");
        }

        protected DataTable BindGrid(String[] photos)
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("/Photos/"));
            FileInfo[] fi2 = di.GetFiles();
            List<FileInfo> fi = new List<FileInfo>();

            //Filter images
            for (int i = 0; i < fi2.Count(); i++)
            {
                for (int j = 0; j < photos.Count() - 1; j++)
                {
                    if (fi2[i].ToString().Equals(photos[j]))
                    {
                        fi.Add(fi2[i]);
                    }
                }
            }

            // Save all paths to the DataTable as the data source.
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Url", typeof(System.String));
            dt.Columns.Add(dc);

            for (int i = 0; i < fi.Count(); i++)
            {
                DataRow dro = dt.NewRow();
                dro[0] = fi[i].Name;
                dt.Rows.Add(dro);
            }
            return dt;
        }

        // Handle the thumbnail image selecting event.
        protected void imgBtn_Click(object sender, EventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            Image1.ImageUrl = ib.ImageUrl;

            Image tempImage = new Image();
            tempImage.ImageUrl = ib.ImageUrl;

            Image tempImage2 = new Image();//maxsizes
            tempImage2.Width = 1000;
            tempImage2.Height = 1000;

            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
            else Image1.Width = tempImage2.Width;

            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
            else Image1.Height = tempImage2.Height;

            DataList1.SelectedIndex = Convert.ToInt32(ib.CommandArgument);
        }        

        protected void Button1_Click(object sender, EventArgs e)//accept
        {
            String comment = TextBox9.Text;
            String photos = "";
            String photosold = "";
            photosold = dta.GetSingleDataByID(Int32.Parse(p_id))[0]["photo_after"].ToString();

            try
            {
                HttpFileCollection hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength > 0)
                    {
                        hpf.SaveAs(Server.MapPath("Photos") + "\\" + userID + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString()) + "_" + (System.IO.Path.GetFileName(hpf.FileName)));
                        photos += userID + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString()) + "_" + (System.IO.Path.GetFileName(hpf.FileName)) + "|";
                    }
                }

                if (photos == "") photos = photosold;
                else {
                    String data = photosold;
                    String[] photo = data.Split('|');
                    for (int i = 0; i < photo.Length - 1; i++)//delete photos
                    {
                        try
                        {
                            FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photo[i]);
                            if (TheFile.Exists)
                            {
                                File.Delete(Server.MapPath("Photos") + "\\" + photo[i]);
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.Message);
                        }
                    }
                }
                
                dta.UpdateQuery2(photos,comment,Int32.Parse(p_id));
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Отправлено');");
                Response.Write("document.location.href='supervisor.aspx';");
                Response.Write("</script>");

                //send email from supervisor to rabotnik and admin
                DataTable table = sta.GetDataByType(2);
                String aEmail = table.Rows[0]["email"].ToString();
                String uEmail = TextBox222.Text;
                /*
                sendEmail(uEmail, "Заявка выполнена", "Здравствуйте," + "\r\n" +
                                                        "информируем вас о том, что все замечания по вашей заявке" + "\r\n" + 
                                                        "в Системе Beer Request были устранены." + "\r\n" +
                                                        "Тема вашей заявки: " + TextBox1.Text);


                sendEmail(aEmail, "Закрытие заявки " + TextBox1.Text, "Здравствуйте," + "\r\n" +
                                                        "в Системе Beer Request была выполнена заявка." + "\r\n" +
                                                        "Название торговой точки: " + TextBox3.Text + "\r\n" +
                                                        "Адрес торговой точки: " + TextBox6.Text + "\r\n" +
                                                        "Описание:" + "\r\n" + TextBox2.Text);*/
                //-------------- Посыл майло ------------------------------------------------
                MailAlert Message = new MailAlert();

                /*//Инициатору
                MailAlert.AlertList[] Alert = new MailAlert.AlertList[4];
                Alert[0].ParamName = "@TTName@";    Alert[0].ParamValue = TextBox3.Text;
                Alert[1].ParamName = "@TTAddress@"; Alert[1].ParamValue = TextBox6.Text;
                Alert[2].ParamName = "@Subject@";   Alert[2].ParamValue = TextBox1.Text;
                Alert[3].ParamName = "@Desc@";      Alert[3].ParamValue = TextBox2.Text;
                Message.SendAlert("close", uEmail, "Заявка выполнена", Alert).ToString();*/

                //Координатору
                MailAlert.AlertList[] Alert1 = new MailAlert.AlertList[8];
                Alert1[0].ParamName = "@TTName@";       Alert1[0].ParamValue = TextBox3.Text;
                Alert1[1].ParamName = "@TTAddress@";    Alert1[1].ParamValue = TextBox6.Text;
                Alert1[2].ParamName = "@Subject@";      Alert1[2].ParamValue = TextBox1.Text;
                Alert1[3].ParamName = "@Desc@";         Alert1[3].ParamValue = TextBox2.Text;
                Alert1[4].ParamName = "@S_FIO@";        Alert1[4].ParamValue = TextBox5.Text;
                Alert1[5].ParamName = "@S_PhoneNumber@";Alert1[5].ParamValue = TextBox333.Text;
                Alert1[6].ParamName = "@S_Email@";      Alert1[6].ParamValue = HyperLink1.Text;
                Alert1[7].ParamName = "@K_Desc@";       Alert1[7].ParamValue = TextBox9.Text;
                Message.SendAlert("close_k", aEmail, "Заявка выполнена", Alert1).ToString();
                //---------------------------------------------------------------------------


            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        void sendEmail(string mailto, string subject, string body)
        {
            try
            {
                mydataTableAdapter mta = new mydataTableAdapter();
                string email = mta.GetData().Rows[0]["email"].ToString();
                string password = mta.GetData().Rows[0]["password"].ToString();
                MailMessage objMail = new MailMessage(email, mailto, subject, body);
                NetworkCredential objNC = new NetworkCredential(email, password);
                SmtpClient objsmtp = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTP"), Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort")));
                objsmtp.EnableSsl = true;
                objsmtp.Credentials = objNC;
                objsmtp.Send(objMail);
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'A' && str[i] <= 'z'))
                    sb.Append(str[i]);
            }

            return sb.ToString();
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private string SplitString(String str)
        {

            if (str.Length > 10)
            {
                string s1 = str.Substring(0, 5);
                string reverse = Reverse(str);
                string s2_ = reverse.Substring(0, 5);
                string s2 = Reverse(s2_);
                s1 += s2;
                return s1;
            }
            return str;
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            /*
            if (TextBox7.Text != "")
            {
                
                DataTable dt = dta.GetTT(TextBox7.Text);
                ArrayList al = new ArrayList();

                foreach (DataRow row in dt.Rows)
                {
                    al.Add(row["theme"].ToString());
                }
                DataList3.DataSource = al;
                DataList3.DataBind();
            */

            if (TextBox7.Text != "")
            {
                G11.DataSource = dta.GetTT(TextBox7.Text);
                G11.DataBind();
            }
        }

     
        protected void GridView1PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (TextBox7.Text != "")
            {
                G11.PageIndex = e.NewPageIndex;
                G11.DataSource = dta.GetTT(TextBox7.Text);
                G11.DataBind();
            }
        }

        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/details.aspx");
        }
    }
}