﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;

namespace Carlsberg_new
{
    public partial class print : System.Web.UI.Page//supervisor
    {
        string userFIO = "";
        string reportType = "";
        static string date1 = "";
        static string date2 = "";

        static DataTable1TableAdapter dta = new DataTable1TableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userFIO = Request.QueryString["user"];
                reportType = Request.QueryString["type"];
                date1 = Request.QueryString["date1"];
                date2 = Request.QueryString["date2"];
                
                if (userFIO != null && reportType != null)
                {                  
                    label1.Text = userFIO;

                    label6.Text = date1;
                    label8.Text = date2;

                    if (reportType.Equals("0"))
                    {//zakrytye
                        GridView1.DataSource = dta.GetClosedProblems(User.Identity.Name.ToString(),Convert.ToDateTime(date1),Convert.ToDateTime(date2));
                        GridView1.DataBind();
                        GridView1.Columns[4].Visible = false;
                        label2.Text = "закрытым заявкам";
                    }
                    if (reportType.Equals("1"))
                    {//открытые свои
                            GridView1.DataSource = dta.GetUnclosedProblems(User.Identity.Name.ToString(),Convert.ToDateTime(date1),Convert.ToDateTime(date2));
                            GridView1.DataBind();
                            GridView1.Columns[4].Visible = false;
                            label2.Text = "открытым заявкам";
                    }
                    
                    if (reportType.Equals("2"))
                    {//все свои
                        GridView1.DataSource = dta.GetDataForSupevisorALL(User.Identity.Name.ToString(), Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                        GridView1.DataBind();
                        label2.Text = "всем заявкам";

                        int rows = GridView1.Rows.Count;
                        for (int i = 0; i < rows; i++)
                        {
                            if (GridView1.Rows[i].Cells[4].Text == "3") GridView1.Rows[i].Cells[4].Text = "<b style=color:blue>Закрытая</b>";
                            if (GridView1.Rows[i].Cells[4].Text == "1" || GridView1.Rows[i].Cells[4].Text == "2") GridView1.Rows[i].Cells[4].Text = "<b style=color:green>Открытая</b>";
                            if (GridView1.Rows[i].Cells[4].Text == "0") GridView1.Rows[i].Cells[4].Text = "<b style=color:red>Неназначенная</b>";
                            //GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
                        }
                    }                           
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Print", "javascript:window.print();", true);
                }
                else Response.Redirect("supervisor.aspx");
            }
            else Response.Redirect("default.aspx");
        }
    }
}