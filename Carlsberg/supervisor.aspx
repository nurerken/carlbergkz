﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="supervisor.aspx.cs" Inherits="Carlsberg_new.supervisor" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <asp:LoginView ID="LoginView1" runat="server">  
      <LoggedInTemplate>  
         <table>
            <tr>
            <td style="width:400px"></td>
            <td style="width:400px"></td>
            <td style="width:400px"></td>
                <td style="width:100px">
                    <asp:HyperLink runat="server" navigateURL="EditProfile.aspx">Мои данные</asp:HyperLink>
                </td>
            </tr>
        </table>
      <ul class="nav nav-tabs">
       <li id = "new" class="active"><a href="#new1" data-toggle="tab" >Новые заявки</a></li>
       <li id = "all"><a href="#all1" data-toggle="tab">Все заявки</a></li>
      </ul>

     <div class="tab-content" id="mytabcontent">
        <div class="tab-pane active" id="new1">
            <asp:GridView ID="GridView2" runat="server" AllowPaging="True"  PageSize="30"
                    CellPadding="4" GridLines="None" OnPageIndexChanging = "GridViewPageIndexChanging"
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                           <asp:TemplateField HeaderText = "&#1058;&#1077;&#1084;&#1072;" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton2"                                     
                                    Text='<%# Eval("theme") %>'              
                                    OnClick="LinkButton_Click2" 
                                    CommandArgument=<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="workerFIO" HeaderText="&#1060;&#1048;&#1054; &#1087;&#1086;&#1076;&#1072;&#1074;&#1096;&#1077;&#1075;&#1086; &#1079;&#1072;&#1103;&#1074;&#1082;&#1091;" 
                                SortExpression="workerFIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="FIO" HeaderText="&#1054;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1077;&#1085;&#1085;&#1099;&#1081;" 
                                SortExpression="FIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="address" HeaderText="&#1040;&#1076;&#1088;&#1077;&#1089;" SortExpression="address" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="date" HeaderText="&#1044;&#1072;&#1090;&#1072;" SortExpression="date" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="p_type" HeaderText="&#1057;&#1090;&#1072;&#1090;&#1091;&#1089;" SortExpression="p_type" 
                                ItemStyle-Width="70px">
                <ItemStyle Width="70px"></ItemStyle>
                            </asp:BoundField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>   
        </div>
         <div class="tab-pane" id="all1">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

    Сделать отчет по
        <asp:DropDownList ID = "DropDownList1" runat="server" style="width:100px"></asp:DropDownList>
    заявкам
    <asp:Button ID="Button3" runat="server" Text="Печать" BackColor="White" 
            ForeColor="#0099FF"  OnClick = "printClosed"/>
    
    <br/>

         <table style="height:300px; width:500px">
           <tr>
            <td><asp:Label ID="Label11112" runat="server" Text="От:" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="Label11141" runat="server" Text="До:" Font-Bold="True"></asp:Label></td>
           </tr>
           <tr>
            <td>
                <asp:Label ID="Label1111" runat="server" Text="От:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label5555" runat="server" Text="До:"></asp:Label>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC" 
        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
        onselectionchanged="Calendar1_SelectionChanged" ShowGridLines="True" 
        Width="220px">
      <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
      <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
      <OtherMonthDayStyle ForeColor="#CC9966" />
      <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
      <SelectorStyle BackColor="#FFCC66" />
      <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
          ForeColor="#FFFFCC" />
      <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
            </td>
            <td>
             <asp:Calendar ID="Calendar2"  runat="server" BackColor="#FFFFCC" 
        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
        onselectionchanged="Calendar2_SelectionChanged" ShowGridLines="True" 
        Width="220px">
      <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
      <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
      <OtherMonthDayStyle ForeColor="#CC9966" />
      <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
      <SelectorStyle BackColor="#FFCC66" />
      <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
          ForeColor="#FFFFCC" />
      <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
            </td>
          </tr> 
        </table>     
        </ContentTemplate>      
    </asp:UpdatePanel>    
    <asp:Button ID="Button2" runat="server" Text="Показать" OnClick = "btn1_click"/><br/>
     <asp:Label ID="label1" runat="server" Text="Все мои и неназначенные заявки">
     </asp:Label>
            <br/>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="30"
                AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan" 
                BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" 
                OnPageIndexChanging="GridView1PageIndexChanging">
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <Columns>
                    <asp:TemplateField HeaderText="Тема">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton22" runat="server" 
                                CommandArgument="<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>" 
                                OnClick="LinkButton_Click" Text='<%# Eval("theme") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="workerFIO" HeaderText="ФИО подавшего заявку" 
                        ItemStyle-Width="100px" SortExpression="workerFIO">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FIO" HeaderText="Ответственный" 
                        ItemStyle-Width="100px" SortExpression="FIO">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="address" HeaderText="Адрес" ItemStyle-Width="100px" 
                        SortExpression="address">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="date" HeaderText="Дата" ItemStyle-Width="80px" 
                        SortExpression="date">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="p_type" HeaderText="Статус" ItemStyle-Width="70px" 
                        SortExpression="p_type">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" 
                    HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
            </asp:GridView>
                </div>
            </div> 
            </LoggedInTemplate>  

            <AnonymousTemplate>  
               <asp:HyperLink runat="server" navigateURL="default.aspx">Вход на сайт</asp:HyperLink>
            </AnonymousTemplate>  
        </asp:LoginView> 

       
</asp:Content>
