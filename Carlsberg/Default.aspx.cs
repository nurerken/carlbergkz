﻿using System.Configuration;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.IO;

namespace Carlsberg_new
{
    public partial class Default : System.Web.UI.Page
    {
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        string adminEmail = "";
        static string date1 = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                TextBox141.Text = System.DateTime.Today.ToString("dd-MM-yyyy");
                date1 = TextBox141.Text;
            }
            if (Request.IsAuthenticated)
            {
                LoginStatus1_ModalPopupExtender.Enabled = false;
            }
            else
            {
                LoginStatus1_ModalPopupExtender.Enabled = true;
            }            
        }

        // When Login button clicked, authenticate user's crendential.
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            e.Authenticated = false;
            int rows = 0;
            rows = sta.GetDataBy11(filter(Login1.UserName), filter(Login1.Password)).Rows.Count;
            e.Authenticated = (rows==1);

            // If login failed, continue showing the popup dialog and failure text.
            if (!e.Authenticated)
            {
                LoginStatus1_ModalPopupExtender.Show();
            }
            if (e.Authenticated) {
                string type = sta.GetDataBySID(filter(Login1.UserName)).Rows[0]["type"].ToString();
                if (type.Equals("1")){
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName),true);
                    Response.Redirect("supervisor.aspx");
                }
                if (type.Equals("2")) {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName), true);
                    Response.Redirect("admin.aspx"); 
                }
                if (type.Equals("3"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName), true);
                    Response.Redirect("moder.aspx");
                }
                if (type.Equals("4"))
                {
                    FormsAuthentication.SetAuthCookie(filter(Login1.UserName), true);
                    Response.Redirect("browse.aspx");
                }
            }
        }

        // When Cancel button clicked, hide the popup dialog.
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoginStatus1_ModalPopupExtender.Hide();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
                                   
            if (TextBox7.Text != "" & TextBox2.Text != "" & TextBox6.Text != "" & TextBox3.Text != "" & TextBox1.Text != "" & TextBox4.Text != "" & TextBox5.Text != "")
            {
                String userID = TextBox1.Text;
                String photos = "";
                try
                {
                    HttpFileCollection hfc = Request.Files;
                    for (int i = 0; i < hfc.Count; i++)
                    {                    
                        HttpPostedFile hpf = hfc[i];
                        if (hpf.ContentLength > 0)
                        {
                            hpf.SaveAs(Server.MapPath("Photos") + "\\" + userID + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString("dd-MM-yyyy-hh-mm")) + "_" + (System.IO.Path.GetFileName(hpf.FileName)));
                            photos += userID + "_" + RemoveSpecialCharacters(System.DateTime.Now.ToString("dd-MM-yyyy-hh-mm")) + "_" + (System.IO.Path.GetFileName(hpf.FileName)) + "|";
                            //Response.Write("<b>File: </b>" + hpf.FileName + "  <b>Size:</b> " + hpf.ContentLength + "  <b>Type:</b> " + hpf.ContentType + " Uploaded Successfully <br/>");
                        }
                    }
                    dta.InsertQuery((TextBox7.Text), (TextBox2.Text), (TextBox6.Text), photos, Convert.ToDateTime(TextBox141.Text),(TextBox3.Text), (TextBox1.Text), (TextBox4.Text), (TextBox5.Text));
                    
                    //send email
                    DataTable table = sta.GetDataByType(2);
                    adminEmail = table.Rows[0]["email"].ToString();


                    //-------------- Посыл майло ------------------------------------------------
                    MailAlert Message = new MailAlert();

                    MailAlert.AlertList[] Alert = new MailAlert.AlertList[7];
                    Alert[0].ParamName = "@TTName@";        Alert[0].ParamValue = TextBox6.Text;
                    Alert[1].ParamName = "@TTAddress@";     Alert[1].ParamValue = TextBox3.Text;
                    Alert[2].ParamName = "@Subject@";       Alert[2].ParamValue = TextBox2.Text;
                    Alert[3].ParamName = "@Desc@";          Alert[3].ParamValue = TextBox7.Text;
                    Alert[4].ParamName = "@FIO@";           Alert[4].ParamValue = TextBox1.Text;
                    Alert[5].ParamName = "@PhoneNumber@";   Alert[5].ParamValue = TextBox5.Text;
                    Alert[6].ParamName = "@Email@";         Alert[6].ParamValue = TextBox4.Text;

                    Message.SendAlert("new_k", adminEmail, "Новая заявка",Alert).ToString();
                    //---------------------------------------------------------------------------
                    


                    //sendEmail(adminEmail, "Новая заявка", "В систему Beer Request поступила новая заявка. Тема Заявки:" + TextBox2.Text + " \n " + "Адрес ТТ: " + TextBox3.Text + " \n " + "Название ТТ: " + TextBox6.Text + " \n " + "Описание:" + " \n "+ TextBox7.Text);

                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Заявка принята');");
                    Response.Write("document.location.href='default.aspx';");
                    Response.Write("</script>");                    
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }                               
            }
            
            else {
                Response.Write("Vse polia obiazatelny dlia zapolnenia");
            } 
        }
        
        void sendEmail(string mailto, string subject, string body)
        {
            try
            {
                mydataTableAdapter mta = new mydataTableAdapter();
                string email = mta.GetData().Rows[0]["email"].ToString();
                string password = mta.GetData().Rows[0]["password"].ToString();
                MailMessage objMail = new MailMessage(email, mailto, subject, body);
                NetworkCredential objNC = new NetworkCredential(email, password);
                SmtpClient objsmtp = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTP"), Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort")));
                objsmtp.EnableSsl = true;
                objsmtp.Credentials = objNC;
                objsmtp.Send(objMail);
            }
            catch (Exception ex) { 
                Response.Write(ex.Message); 
            }
        }

        string filter(string txt) {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'A' && str[i] <= 'z'))
                    sb.Append(str[i]);
            }

            return sb.ToString();
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private string SplitString(String str) {
            
            if (str.Length > 30)
            {
                string s1 = str.Substring(0, 10);
                string reverse = Reverse(str);
                string s2_ = reverse.Substring(0, 10);
                string s2 = Reverse(s2_);
                s1 += s2;
                return s1;                
            }
            return str;
        }

        protected void TextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("myrequests.aspx");
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            date1 = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
            TextBox141.Text = date1;
           // Calendar1.Visible = false;

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
       /*     if (Calendar1.Visible == true)
            {
            Calendar1.Visible = false;
            }else {
                Calendar1.Visible = true;
            }*/
        }
    }
}