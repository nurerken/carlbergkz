﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Net.Mail;




namespace Carlsberg_new
{
    public class MailAlert
    {

        public struct AlertList
        {
            public string ParamName;
            public string ParamValue;
        }

        public Int32 SendAlert(string Template, string To, string Subject, AlertList[] Alert)
        {
            StreamReader s = File.OpenText(HttpContext.Current.Server.MapPath("Mail") + "\\" + Template + ".htm");
            string read = null;
            read = s.ReadToEnd();
            s.Close();
            String val;


            foreach (AlertList i in Alert)
            {
                val = i.ParamValue;
                val = val.Replace("\r\n", "<br>");
                read = read.Replace(i.ParamName, val);
            }

            var MailClient = new SmtpClient();

            /*var MailClient = new SmtpClient("smtp.mail.ru", 587);
            var Credential = new NetworkCredential("andrey_1c", "andreySALKOV020284");
            MailClient.Credentials = Credential;
            */


            var Message = new MailMessage { Subject = Subject };
            Message.To.Add(To);

            Message.BodyEncoding = Encoding.UTF8;
            Message.HeadersEncoding = Encoding.UTF8;
            Message.SubjectEncoding = Encoding.UTF8;
            Message.IsBodyHtml = true;



            if (Boolean.Parse(ConfigurationManager.AppSettings.Get("isMailHeader")) == true)
            {
                read = read.Replace("@HeaderImg@", "<img id=\"header.jpg\" src=\"cid:header.jpg\">");
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(read, null, "text/html");
                LinkedResource imagelink = new LinkedResource(HttpContext.Current.Server.MapPath("Mail") + "\\header.jpg", "image/jpg");
                imagelink.ContentId = "header.jpg";
                imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                htmlView.LinkedResources.Add(imagelink);
                Message.AlternateViews.Add(htmlView);

            }
            else
            {
                read = read.Replace("@HeaderImg@", "");
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(read, null, "text/html");
                Message.AlternateViews.Add(htmlView);
            }




            MailClient.Send(Message);

            return 0;
        }
    }
}