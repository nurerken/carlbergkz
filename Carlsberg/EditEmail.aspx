﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EditEmail.aspx.cs" Inherits="Carlsberg_new.EditEmail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:Panel ID="Panel1" runat="server" >    
 <input type="button" value="Назад" onclick="history.back()">
      <table>  
        <tr>
            <td>
                <asp:Label ID="lable11" runat="server" Text="Email"></asp:Label>
            </td>
            <td>
                <asp:TextBox id="tb11" runat="server"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                    ControlToValidate="tb11" ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                    ControlToValidate="tb11" ErrorMessage="неправильный email адрес" 
                    
                    ValidationExpression="^(?(&quot;&quot;)(&quot;&quot;.+?&quot;&quot;@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&amp;'\*\+/=\?\^`\{\}\|~\w])*)(?&lt;=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"></asp:RegularExpressionValidator>
            </td>
        </tr>
      
        <tr>            
            <td>
                <asp:label ID="lable22" runat="server" text="Новый пароль"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb22" runat="server" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                    ControlToValidate="tb22" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:label ID="lable33" runat="server" text="Подтвердить новый пароль"></asp:label>
            </td>
            <td>
                <asp:TextBox id="tb33" runat="server" type="password"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" 
                    ControlToValidate="tb33" ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="tb22" ControlToValidate="tb33" 
                    ErrorMessage="Пароли не совпадают"></asp:CompareValidator>
            </td>
        </tr>
      </table>
    <asp:button id="button11" runat="server" text="Изменить" 
            onclick="button11_Click" onclientclick="return check();"/>
    </asp:Panel>

    <script type="text/javascript">
     function check() {
            //3,6,2,7
            var t3 = document.getElementById("ContentPlaceHolder1_tb11").value;//email
            var t6 = document.getElementById("ContentPlaceHolder1_tb22").value;//parol
            var t2 = document.getElementById("ContentPlaceHolder1_tb33").value;//parol2
            

            if (t6.toString() == "") {
                alert("пожалуйста, заполните поле пароль");
                return false;
            }
            if (t3.toString() == "") {
                alert("пожалуйста, заполните поле email");
                return false;
            }            
            if (t2.toString() == "") {
                alert("пожалуйста, заполните поле пароль");
                return false;
            }
            if (t2.toString() != t6.toString()) {
                alert("пароли не совпадают");
                return false;
            }
            return true;
        }
          </script>
</asp:Content>
