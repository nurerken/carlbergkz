﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;
using System.IO;

namespace Carlsberg_new
{
    public partial class moder : System.Web.UI.Page
    {
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        static mydataTableAdapter mta = new mydataTableAdapter();
        static string date1 = System.DateTime.Today.ToString("yyyy-MM-1");
        static string date2 = System.DateTime.Today.ToString("yyyy-MM-dd");
        static bool b1 = false;
        static bool b2 = false;
        string adminID="";
        static bool b = false;
        static List<string> pIDs = new List<string>();
        string userType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                adminID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(adminID).Rows[0]["type"].ToString();
                
                if (userType.Equals("3"))
                {
                    if (!IsPostBack)
                    {
                        if (User.Identity.IsAuthenticated)
                        {
                            Label LabelDo = (Label)LoginView1.FindControl("LabelDo");
                            Label LabelPosle = (Label)LoginView1.FindControl("LabelPosle");

                            LabelDo.Text = date1;
                            LabelPosle.Text = date2;

                            GridView GridView1 = (GridView)LoginView1.FindControl("GridView1");
                            GridView GridView11 = (GridView)LoginView1.FindControl("GridView11");

                            GridView GridView2 = (GridView)LoginView1.FindControl("GridView2");

                            GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                            GridView GridViewSpectator = (GridView)LoginView1.FindControl("GridViewSpectator");

                            GridView1.DataSource = sta.GetDataByType(2);//adminy
                            GridView1.DataBind();

                            GridViewSpectator.DataSource = sta.GetDataByType(4);//spectator
                            GridViewSpectator.DataBind();

                            GridView11.DataSource = sta.GetSupervisors("0");//supervisors
                            GridView11.DataBind();

                            GridView2.DataSource = sta.GetDataByType(3);//moi dannye
                            GridView2.DataBind();

                            GridView3.DataSource = dta.GetDataFromFirstDay();//zaiavki
                            GridView3.DataBind();

                            int rows = GridView3.Rows.Count;

                            for (int i = 0; i < rows; i++)
                            {
                                if (GridView3.Rows[i].Cells[6].Text == "3")
                                {
                                    GridView3.Rows[i].Cells[6].Text = "<b style=color:blue>Закрытая</b>";
                                   
                                }
                                if (GridView3.Rows[i].Cells[6].Text.Equals("1") || GridView3.Rows[i].Cells[6].Text == "2")
                                {
                                    GridView3.Rows[i].Cells[6].Text = "<b style=color:green>Открытая</b>";                                    
                                }
                                if (GridView3.Rows[i].Cells[6].Text == "0")
                                {
                                    GridView3.Rows[i].Cells[6].Text = "<b style=color:red>Неназначенная</b>";                                    
                                }

                                GridView3.Rows[i].Cells[5].Text = (GridView3.Rows[i].Cells[5].Text).Substring(0, 10);                                
                            }

                            Label Label123 = (Label)LoginView1.FindControl("Label123");
                            Label123.Text = mta.GetData().Rows[0]["email"].ToString();
                        }
                    }
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
            else Response.Redirect("default.aspx");
        }

        protected void Check_Clicked(Object sender, EventArgs e)
        {
            string p_id = (sender as CheckBox).ToolTip;
            if ((sender as CheckBox).Checked)
                pIDs.Add(p_id);
            else
                pIDs.Remove(p_id);            
        }
        
        protected void deleteSelected(Object sender, EventArgs e)
        {
            if (pIDs.Count() != 0)
            {                
                string data = "";
                for (int i = 0; i < pIDs.Count(); i++)
                {
                    try
                    {
                        data += dta.GetSingleDataByID(Int32.Parse(pIDs[i])).Rows[0]["photo_before"].ToString();
                        data += dta.GetSingleDataByID(Int32.Parse(pIDs[i])).Rows[0]["photo_after"].ToString();

                        dta.DeleteSingleByID(Int32.Parse(pIDs[i]));
                    }
                    catch (Exception ex) { Response.Write(ex.Message); }
                }

                String[] photo = data.Split('|');
                for (int i = 0; i < photo.Length - 1; i++)//delete photos
                {
                    try
                    {
                        FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photo[i]);
                        if (TheFile.Exists)
                        {
                            File.Delete(Server.MapPath("Photos") + "\\" + photo[i]);
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                }

                int count = pIDs.Count();
                pIDs = new List<string>();

                Response.Write("<script type='text/javascript'>");
                if (count == 1) Response.Write("alert('удаленa " + count + " заявкa');");
                if (count == 2) Response.Write("alert('удалены " + count + " заявки');");
                if (count == 3) Response.Write("alert('удалены " + count + " заявки');");
                if (count == 4) Response.Write("alert('удалены " + count + " заявки');");
                else if (count > 4) Response.Write("alert('удалены " + count + " заявок');");
                Response.Write("document.location.href='moder.aspx';");
                Response.Write("</script>");                

                /*
                if (User.Identity.IsAuthenticated)
                {
                    GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                    GridView3.DataSource = dta.GetDataFromFirstDay();
                    GridView3.DataBind();

                    int rows = GridView3.Rows.Count;

                    for (int i = 0; i < rows; i++)
                    {
                        if (GridView3.Rows[i].Cells[6].Text == "3" || GridView3.Rows[i].Cells[6].Text == "2") GridView3.Rows[i].Cells[6].Text = "<b style=color:blue>Закрыто</b>";
                        else GridView3.Rows[i].Cells[6].Text = "<b style=color:red>Не закрыто</b>";

                        GridView3.Rows[i].Cells[5].Text = (GridView3.Rows[i].Cells[4].Text).Substring(0, 10);
                    }
                }
                */
            }
            else
            {
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('выберите заявки для удаления');");
                Response.Write("</script>");
            }
        }
        
        protected void GridView3PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                GridView3.PageIndex = e.NewPageIndex;
                if (b)
                    GridView3.DataSource = dta.GetDataBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                else
                    GridView3.DataSource = dta.GetDataFromFirstDay();

                GridView3.DataBind();

                int rows = GridView3.Rows.Count;
                for (int i = 0; i < rows; i++)
                {
                    if (GridView3.Rows[i].Cells[6].Text == "3")
                    {
                        GridView3.Rows[i].Cells[6].Text = "<b style=color:blue>Закрытая</b>";

                    }
                    if (GridView3.Rows[i].Cells[6].Text.Equals("1") || GridView3.Rows[i].Cells[6].Text == "2")
                    {
                        GridView3.Rows[i].Cells[6].Text = "<b style=color:green>Открытая</b>";
                    }
                    if (GridView3.Rows[i].Cells[6].Text == "0")
                    {
                        GridView3.Rows[i].Cells[6].Text = "<b style=color:red>Неназначенная</b>";
                    }

                    GridView3.Rows[i].Cells[5].Text = (GridView3.Rows[i].Cells[5].Text).Substring(0, 10);
                }

                System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
                sb22.Append("<script language=\"javascript\">");
                sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
                sb22.Append("document.getElementById(\"ready\").className=\"passivedl\";");
                sb22.Append("document.getElementById(\"all\").className=\"active\";");
                sb22.Append("document.getElementById(\"inbox\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"outbox\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"newmess\").className=\"tab-pane active\";");
                sb22.Append("</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());
            }
        }

        protected void LinkButton_Click2(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("edit1.aspx");
        }

        protected void LinkButton_Click3(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("details.aspx");//, "_blank", "menubar=0,scrollbars=1,width=900,height=900,top=10");
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Calendar Calendar1 = (Calendar)LoginView1.FindControl("Calendar1");
                Label LabelDo = (Label)LoginView1.FindControl("LabelDo");
                date1 = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
                LabelDo.Text = date1;
                b1 = true;
            }
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Calendar Calendar2 = (Calendar)LoginView1.FindControl("Calendar2");
                Label LabelPosle = (Label)LoginView1.FindControl("LabelPosle");
                date2 = Calendar2.SelectedDate.ToString("yyyy-MM-dd");
                LabelPosle.Text = date2;
                b2 = true;
            }
        }

        protected void btn1_click(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
                sb22.Append("<script language=\"javascript\">");
                sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
                sb22.Append("document.getElementById(\"ready\").className=\"passivedl\";");
                sb22.Append("document.getElementById(\"all\").className=\"active\";");
                sb22.Append("document.getElementById(\"inbox\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"outbox\").className=\"tab-pane\";");
                sb22.Append("document.getElementById(\"newmess\").className=\"tab-pane active\";");
                sb22.Append("</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());

                GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                GridView3.DataSource = dta.GetDataBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                GridView3.DataBind();

                b = true;
                int rows = GridView3.Rows.Count;

                for (int i = 0; i < rows; i++)
                {
                    if (GridView3.Rows[i].Cells[6].Text == "3" || GridView3.Rows[i].Cells[6].Text == "2")
                    {
                        GridView3.Rows[i].Cells[6].Text = "<b style=color:blue>Закрытая</b>";

                    }
                    if (GridView3.Rows[i].Cells[6].Text.Equals("1"))
                    {
                        GridView3.Rows[i].Cells[6].Text = "<b style=color:green>Открытая</b>";
                    }
                    if (GridView3.Rows[i].Cells[6].Text == "0")
                    {
                        GridView3.Rows[i].Cells[6].Text = "<b style=color:red>Неназначенная</b>";
                    }

                    GridView3.Rows[i].Cells[5].Text = (GridView3.Rows[i].Cells[5].Text).Substring(0, 10);
                }
            }
        }

        protected void btn2_click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
            sb22.Append("<script language=\"javascript\">");
            sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
            sb22.Append("document.getElementById(\"ready\").className=\"passivedl\";");
            sb22.Append("document.getElementById(\"all\").className=\"active\";");
            sb22.Append("document.getElementById(\"inbox\").className=\"tab-pane\";");
            sb22.Append("document.getElementById(\"outbox\").className=\"tab-pane\";");
            sb22.Append("document.getElementById(\"newmess\").className=\"tab-pane active\";");
            sb22.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());
            
            if (!b1)
            {
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Пожалуйста, выберите начальную дату');");
                Response.Write("</script>");
                return;
            }
            if (!b2)
            {
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Пожалуйста, выберите конечную дату');");
                Response.Write("</script>");
                return;
            }
            if (b1 & b2)
            {
                try
                {
                    DataTable table1 = dta.GetDataBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                    String data = "";

                    foreach (DataRow row in table1.Rows)
                    {
                        data += row["photo_before"].ToString();
                        data += row["photo_after"].ToString();
                    }

                    String[] photo = data.Split('|');
                    for (int i = 0; i < photo.Length - 1; i++)//delete photos
                    {
                        try
                        {
                            FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photo[i]);
                            if (TheFile.Exists)
                            {
                                File.Delete(Server.MapPath("Photos") + "\\" + photo[i]);
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.Message);
                        }
                    }
                    //delete rows
                    int cnt = Int32.Parse(dta.SelectCountBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2)).ToString());
                    dta.DeleteQuery(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                    
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('" + cnt + " заявки(ок) успешно удалены');");                   
                    Response.Write("document.location.href='moder.aspx';");
                    Response.Write("</script>");
                    
                    /*
                    if (User.Identity.IsAuthenticated)
                    {
                        GridView GridView3 = (GridView)LoginView1.FindControl("GridView3");
                        GridView3.DataSource = dta.GetDataFromFirstDay();
                        GridView3.DataBind();

                        int rows = GridView3.Rows.Count;

                        for (int i = 0; i < rows; i++)
                        {
                            if (GridView3.Rows[i].Cells[6].Text == "3" || GridView3.Rows[i].Cells[6].Text == "2") GridView3.Rows[i].Cells[6].Text = "<b style=color:blue>Закрыто</b>";
                            else GridView3.Rows[i].Cells[6].Text = "<b style=color:red>Не закрыто</b>";

                            GridView3.Rows[i].Cells[5].Text = (GridView3.Rows[i].Cells[4].Text).Substring(0, 10);
                        }
                    }
                    */
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }

        protected void btn3_click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb22 = new System.Text.StringBuilder();
            sb22.Append("<script language=\"javascript\">");
            sb22.Append("document.getElementById(\"new\").className=\"passisdfve\";");
            sb22.Append("document.getElementById(\"ready\").className=\"passivedl\";");
            sb22.Append("document.getElementById(\"all\").className=\"active\";");
            sb22.Append("document.getElementById(\"inbox\").className=\"tab-pane\";");
            sb22.Append("document.getElementById(\"outbox\").className=\"tab-pane\";");
            sb22.Append("document.getElementById(\"newmess\").className=\"tab-pane active\";");
            sb22.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb22.ToString());

            
            if (!b1) {
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Пожалуйста, выберите начальную дату');");
                Response.Write("</script>");
                return;
            }
            if (!b2)
            {
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Пожалуйста, выберите конечную дату');");
                Response.Write("</script>");
                return;
            }
            if (b1 & b2) { 
                try
                {
                    //delete query
                    DataTable table1 = dta.GetDataBetweenDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                    String data = "";

                    foreach (DataRow row in table1.Rows) {
                        data += row["photo_before"].ToString();
                        data += row["photo_after"].ToString();
                    }

                    String[] photo = data.Split('|');
                    for (int i = 0; i < photo.Length - 1; i++)//delete photos
                    {
                        try
                        {
                            FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photo[i]);
                            if (TheFile.Exists)
                            {
                                File.Delete(Server.MapPath("Photos") + "\\" + photo[i]);
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.Message);
                        }
                    }

                    int p_id;
                    foreach (DataRow row in table1.Rows) { 
                        p_id = Int32.Parse(row["p_id"].ToString());
                        dta.deletePhotosQuery(p_id);
                    }

                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Выбранные фотографий успешно удалены');");
                    Response.Write("</script>");                    
                }
                catch (Exception ex) {
                    Response.Write(ex.Message);
                }
            }

        }

        protected void bt1click(object sender, EventArgs e)
        {
            Response.Redirect("EditProfile.aspx");
        }

        protected void bt1click2(object sender, EventArgs e)
        {
            Response.Redirect("EditEmail.aspx");
        }

        protected void create(object sender, EventArgs e)
        {
            String type = (sender as Button).CommandArgument;
            Session["usertype"] = type;
            Response.Redirect("createUser.aspx");
        }

        protected void deleteuser(object sender, EventArgs e)
        {
            string s_id = (sender as Button).CommandArgument;
            try
            {
                sta.DeleteQuery(s_id);
                //Response.Write("<script type='text/javascript'>");
                //Response.Write("alert('Супервайзор удален');");
                //Response.Write("document.loation.href='moder.aspx';");
                //Response.Write("</script>");
                Response.Write("Супервайзор удален");
                Response.Redirect("moder.aspx");

            }
            catch (Exception ex) { Response.Write(ex.Message); }            
        }

        protected void otstavka(object sender, EventArgs e)
        {
            string s_id = (sender as Button).CommandArgument;
            try
            {
                try
                {
                    sta.UpdateStatus(s_id);
                    Response.Write("Супервайзор отправлен в отставку");
                    Response.Redirect("moder.aspx");
                }catch(Exception ex){
                    Response.Write(ex.Message);
                }
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        protected void reset(object sender, EventArgs e)
        { 
            TextBox TextBox1 = (TextBox)LoginView1.FindControl("TextBox1");
            if (TextBox1.Text != "") 
            {
                try
                {
                    string user = "";
                    user = sta.GetDataBySID(TextBox1.Text)[0]["FIO"].ToString();

                    if (user != "")
                    {
                        sta.UpdateQuery("123", TextBox1.Text);
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Пароль был сброшен на 123');");
                        Response.Write("document.location.href='moder.aspx';");
                        Response.Write("</script>");
                    }
                    else 
                    {
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Нету такого пользователя');");
                        Response.Write("</script>");    
                    }

                 }
                catch (Exception ex) { Response.Write(ex.Message); }
            }
        }
    }

    public static class ResponseHelper
    {
        public static void Redirect(this HttpResponse response, string url, string target, string windowFeatures)
        {

            if ((String.IsNullOrEmpty(target) || target.Equals("_self", StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
            {
                response.Redirect(url);
            }
            else
            {
                Page page = (Page)HttpContext.Current.Handler;

                if (page == null)
                {
                    throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
                }
                url = page.ResolveClientUrl(url);

                string script;
                if (!String.IsNullOrEmpty(windowFeatures))
                {
                    script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
                }
                else
                {
                    script = @"window.open(""{0}"", ""{1}"");";
                }
                script = String.Format(script, url, target, windowFeatures);
                ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
            }
        }
    }
}
