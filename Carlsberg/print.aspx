﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print.aspx.cs" Inherits="Carlsberg_new.print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br/>
        <asp:label ID = "label3" runat="server" text="отчет по"></asp:label>
        &nbsp;<asp:label ID = "label2" runat="server" text="report type"></asp:label>
        &nbsp;<asp:label ID = "label4" runat="server" text="супервайзора"></asp:label>
        &nbsp;<asp:label ID = "label1" runat="server" text="username" Font-Bold="True"></asp:label>
        &nbsp;<asp:label ID = "label5" runat="server" text="с"></asp:label>
        &nbsp;<asp:label ID = "label6" runat="server" text="date1"></asp:label>
        &nbsp;<asp:label ID = "label7" runat="server" text="по"></asp:label>
        &nbsp;<asp:label ID = "label8" runat="server" text="date2"></asp:label>
        <br/>
        
        <asp:GridView ID="GridView1" runat="server" AllowPaging="False" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                          <asp:BoundField DataField="theme" HeaderText="Тема" ItemStyle-Width="100px" >
                                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>    
                             
                            <asp:BoundField DataField="date" HeaderText="дата подачи заявки" ItemStyle-Width="80px">
                             <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>  
                             
                           <asp:BoundField DataField="nazvanie" HeaderText="название торговой точки" ItemStyle-Width="100px" >
                            <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>     
                                                
                            <asp:BoundField DataField="address" HeaderText="адрес торговой точки" ItemStyle-Width="100px" >
                            <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>   
                            
                            <asp:BoundField DataField="p_type" HeaderText="статус" ItemStyle-Width="100px" >
                            <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>   
                                                 
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
    </div>
    </form>
</body>
</html>
