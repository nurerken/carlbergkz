﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;

namespace Carlsberg_new
{
    public partial class createUser : System.Web.UI.Page
    {
        string userType = "";
        string adminID = "";
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        int type = 0;
                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                adminID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(adminID).Rows[0]["type"].ToString();

                if (userType.Equals("3") && Session["usertype"]!=null)
                {
                    if (Session["usertype"].ToString().Equals("supervisor"))
                    {
                        label1.Text = "Создать нового супервайзора";
                        type = 1;
                    }
                    if (Session["usertype"].ToString().Equals("admin")) 
                    {
                        label1.Text = "Создать нового администратора";
                        type = 2;
                    }                    
                    if (Session["usertype"].ToString().Equals("moder"))
                    {
                        label1.Text = "Создать нового модератора";
                        type = 3;
                    }
                                        
                    if (Session["usertype"].ToString().Equals("spec"))
                    {
                        label1.Text = "Создать нового спектатора";
                        type = 4;
                    }
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

        protected void button1_Click(object sender, EventArgs e)
        {
            try 
            {
                if (type == 1)
                {
                    sta.NewSupervisor(tb1.Text, tb2.Text, tb3.Text, tb5.Text, tb4.Text);
                }
                else {
                    sta.CreateNewAdminOrModer(tb1.Text, tb2.Text, tb3.Text, "1", tb5.Text, tb4.Text, type);
                }
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Пользователь успешно создан');");
                Response.Write("document.location.href='moder.aspx';");
                Response.Write("</script>");
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

    }
}