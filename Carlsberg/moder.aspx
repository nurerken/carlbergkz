﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="moder.aspx.cs" Inherits="Carlsberg_new.moder" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function confirm0() {
            var result = confirm('Вы дейсвтвительно хотите удалить супервайзора?');
            if (result)
                return true;
            else
                return false;
        }

        function confirm1() {
            var result = confirm('Вы дейсвтвительно хотите удалить выбранные заявки?');
            if (result)
                return true;
            else
                return false;
        }

        function confirm2() {
            var result = confirm('Вы дейсвтвительно хотите удалить выбранные фотографий?');
            if (result)
                return true;
            else
                return false;
        }

        function c1() {
            var result = confirm('Вы дейсвтвительно хотите отправить супервайзора в отставку?');
            if (result)
                return true;
            else
                return false;
        }
  </script>

    <asp:loginview ID = "LoginView1" runat="server">
    <AnonymousTemplate>
        <asp:HyperLink runat="server" navigateURL="default.aspx">Вход на сайт</asp:HyperLink>
    </AnonymousTemplate>
    
    <LoggedInTemplate>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <ul class="nav nav-tabs">
      <li id = "all" class="active"><a href="#newmess" data-toggle="tab">Управление заявками</a></li>
      <li id = "new"><a href="#inbox" data-toggle="tab" >Управление пользователями</a></li>
      <li id = "ready"><a href="#outbox" data-toggle="tab">Мои данные</a></li>
    </ul>

   <div class="tab-content" id="mytabcontent">
    <div class="tab-pane" id="inbox">
        <asp:Label ID="dsfdsafl" runat="server" Text="Администратор:"></asp:Label><br/>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="False" 
                    CellPadding="4" GridLines="None" AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                            <asp:BoundField DataField="s_id" HeaderText="ID" 
                                 ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="FIO" HeaderText="ФИО" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="email" HeaderText="email" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="phone" HeaderText="номер телефона" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>  
                    <br/> 
            <asp:Button ID="CreateAdminButton" runat="server" Text="Изменить администратора" onClick="create" CommandArgument="admin"></asp:Button>
            <hr/>




            <asp:Label ID="dsfdsaf32l" runat="server" Text="Спектатор:"></asp:Label><br/>
        <asp:GridView ID="GridViewSpectator" runat="server" AllowPaging="False" 
                    CellPadding="4" GridLines="None" AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                            <asp:BoundField DataField="s_id" HeaderText="ID" 
                                 ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="FIO" HeaderText="ФИО" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="email" HeaderText="email" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="phone" HeaderText="номер телефона" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>  
                    <br/> 
            <asp:Button ID="CreateSpectatorButton" runat="server" Text="Изменить спектатора" onClick="create" CommandArgument="spec"></asp:Button>
            <hr/>

            

          <asp:Label ID="dsf32" runat="server" Text="Супервайзоры:"></asp:Label><br/>
        <asp:GridView ID="GridView11" runat="server" AllowPaging="False" 
                    CellPadding="4" GridLines="None" AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                            <asp:BoundField DataField="s_id" HeaderText="ID" 
                                 ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="FIO" HeaderText="ФИО" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="email" HeaderText="email" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="phone" HeaderText="номер телефона" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                            
                            <asp:TemplateField HeaderText = "Изменить статус" >
                                <ItemTemplate>
                                    <asp:Button ID ="b1" runat="server" Text="не работает" ForeColor="Red" onclientclick="return c1();"
                                    onclick = "otstavka"
                                    CommandArgument = <%#((DataSet1.supervisorsRow)(((DataRowView)Container.DataItem).Row)).s_id%> Font-Bold="True" BorderColor="Red"></asp:Button>                                    
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>  
            <asp:Button ID="newSV" runat="server" Text="Создать нового супервайзора" onClick="create" CommandArgument="supervisor"></asp:Button>
            <hr/>
        <asp:Label ID="Label2" runat="server" Text="<b>Сброс пароля</b>"></asp:Label><br/>
        <asp:Label ID="Label1" runat="server" Text="Введите userID:"></asp:Label> <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br/>
            <asp:Button ID="Button4" runat="server" Text="Сброс" OnClick="reset"/>
    </div>

    <div class="tab-pane" id="outbox">
        <asp:GridView ID="GridView2" runat="server" AllowPaging="False" 
                    CellPadding="4" GridLines="None" 
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                           <asp:BoundField DataField="s_id" HeaderText="ID" 
                                 ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="FIO" HeaderText="ФИО" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>

                           <asp:BoundField DataField="email" HeaderText="email" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="phone" HeaderText="номер телефона" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                            
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <br/>
                    <asp:Button ID="changeProfile" runat="server" Text="Редактировать мои данные" onclick = "bt1click"></asp:Button>
                    <asp:Button ID="button_new" runat="server" Text="Изменить модератора" onclick = "create" CommandArgument="moder"></asp:Button>
                    <hr/>
                    <asp:Label ID="l1" runat="server" Font-Bold="True" Text="default email: "></asp:Label><asp:Label ID="Label123" runat="server"></asp:Label>
                    <br/>
                    <asp:Button ID="changeEmail" runat="server" Text="Редактировать" onclick = "bt1click2"></asp:Button>
                    <hr/>
                    
    </div>    
    <div class="tab-pane active" id="newmess">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
           <table style="height:300px; width:500px">
           <tr>
            <td>
                <asp:Label ID="Label1111" runat="server" Text="От:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label5555" runat="server" Text="До:"></asp:Label>
            </td>
          </tr>

          <tr>
            <td>
                <asp:Label ID="LabelDo" runat="server" Text=""></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelPosle" runat="server" Text=""></asp:Label>
            </td>
          </tr>

          <tr>
            <td>
              <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC" 
        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
        onselectionchanged="Calendar1_SelectionChanged" ShowGridLines="True" 
        Width="220px">
      <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
      <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
      <OtherMonthDayStyle ForeColor="#CC9966" />
      <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
      <SelectorStyle BackColor="#FFCC66" />
      <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
          ForeColor="#FFFFCC" />
      <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
            </td>
            <td>
                  <asp:Calendar ID="Calendar2" runat="server" BackColor="#FFFFCC" 
        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
        onselectionchanged="Calendar2_SelectionChanged" ShowGridLines="True" 
        Width="220px">
      <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
      <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
      <OtherMonthDayStyle ForeColor="#CC9966" />
      <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
      <SelectorStyle BackColor="#FFCC66" />
      <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
          ForeColor="#FFFFCC" />
      <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
            </td>
          </tr> 
        </table>     
        </ContentTemplate>      
    </asp:UpdatePanel> 
    <table>
    <tr>
        <td  style="width:350px">
            <asp:Button ID="Button2" runat="server" Text="Показать заявки по периоду" OnClick = "btn1_click"/>
        </td>
        <td  style="width:400px">
            <asp:Button ID="Button1" runat="server" Text="Удалить заявки по периоду" ForeColor="Red" OnClick = "btn2_click" OnClientClick = "return confirm1();"/>
        </td>
        <td  style="width:400px">
            <asp:Button ID="Button3" runat="server" Text="Удалить фотки по периоду" ForeColor="Red" OnClick = "btn3_click" OnClientClick = "return confirm2();"/>
        </td>
    </tr>
    </table>
         <br/>
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <asp:GridView ID="GridView3" runat="server" AllowPaging="True" PageSize="30"
                    CellPadding="4" GridLines="None" OnPageIndexChanging = "GridView3PageIndexChanging"
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>      
                           
                            <asp:TemplateField HeaderText = "Выбрать" >
                                <ItemTemplate>
                                   <asp:CheckBox ID = "CheckBox1" runat="server" AutoPostBack="True" ToolTip=<%#((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>  oncheckedchanged="Check_Clicked"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                                
                           <asp:TemplateField HeaderText = "&#1058;&#1077;&#1084;&#1072;" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton2"                                     
                                    Text='<%# Eval("theme") %>'              
                                    OnClick="LinkButton_Click3" 
                                    CommandArgument=<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="workerFIO" HeaderText="ФИО подавшего заявку" 
                                SortExpression="workerFIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="FIO" HeaderText="Ответственный" 
                                SortExpression="FIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="address" HeaderText="&#1040;&#1076;&#1088;&#1077;&#1089;" SortExpression="address" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="date" HeaderText="&#1044;&#1072;&#1090;&#1072;" SortExpression="date" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="p_type" HeaderText="&#1057;&#1090;&#1072;&#1090;&#1091;&#1089;" SortExpression="p_type" 
                                ItemStyle-Width="70px">
                <ItemStyle Width="70px"></ItemStyle>
                            </asp:BoundField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                                        
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Button ID = "delteButton" runat="server" ForeColor="Red" Text="Удалить выбранные заявки" onClick="deleteSelected" onclientclick="return confirm1();"></asp:Button>
                    <br/>
                    <hr/> 
                    
                    <asp:Table ID="Table1" runat="server" BackColor="#00FF99">
                    </asp:Table>
        
    </div>    
    </div>    
    </LoggedInTemplate>   
    </asp:loginview>
      
</asp:Content>
