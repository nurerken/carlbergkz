﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Text.RegularExpressions;

namespace Carlsberg_new
{
    public partial class EditEmail : System.Web.UI.Page
    {
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        static mydataTableAdapter mta = new mydataTableAdapter();
        string UserID = "";
        string userType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                UserID = User.Identity.Name;
                userType = sta.GetDataBySID(UserID).Rows[0]["type"].ToString();
                if (userType.Equals("3"))
                {
                    if (!IsPostBack)
                    {
                        tb11.Text = mta.GetData().Rows[0]["email"].ToString();      
                    }

                }
                else
                {
                    Response.Redirect("default.aspx?returnURL=EditEmail.aspx");
                }
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

        protected void button11_Click(object sender, EventArgs e)
        {
            try 
            {
                string email = tb11.Text;
                string pass = filter(tb22.Text);
                string id = mta.GetData().Rows[0]["email"].ToString();

                mta.UpdateQuery(email, pass, id);

                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('Данные изменились');");
                Response.Write("document.location.href='moder.aspx';");
                Response.Write("</script>");                
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        string filter(string txt)
        {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }

    }
}