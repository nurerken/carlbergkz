﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Text.RegularExpressions;
using System.Web.Security;

namespace Carlsberg_new
{
    public partial class EditProfile : System.Web.UI.Page
    {
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        string UserID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                UserID = User.Identity.Name;
                if (!IsPostBack)
                {
                    tb1.Text = sta.GetDataBySID(UserID).Rows[0]["FIO"].ToString();
                    tb2.Text = sta.GetDataBySID(UserID).Rows[0]["email"].ToString();
                    tb3.Text = sta.GetDataBySID(UserID).Rows[0]["phone"].ToString();
                }
            }
            else {
                Response.Redirect("default.aspx?returnURL=EditProfile.aspx");
            }
        }

        protected void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Regex email = new Regex(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
               
                if (email.IsMatch(tb2.Text))
                {
                    sta.UpdateQuery1(filter(tb1.Text), tb2.Text, filter(tb3.Text), filter(UserID));
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Данные изменились');");
                    Response.Write("document.location.href='editprofile.aspx';");
                    Response.Write("</script>");
                }
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        string filter(string txt)
        {
            string str = Regex.Replace(txt, "<.*?>", string.Empty);
            str = HttpContext.Current.Server.HtmlEncode(str);
            return str.Replace("'", string.Empty);
        }

        protected void button11_Click(object sender, EventArgs e)
        {
            if (filter(tb22.Text).Equals(filter(tb33.Text))) {
                int rows = 0;
                rows = sta.GetDataBy11(UserID, filter(tb11.Text)).Rows.Count;
                if (rows == 1)
                {
                    try
                    {
                        sta.UpdatePassword(filter(tb22.Text), UserID);
                        FormsAuthentication.SignOut();
                        Session.Clear();
                        Response.Write("<script type='text/javascript'>");
                        Response.Write("alert('Пароль был изменен');");
                        Response.Write("document.location.href='default.aspx';");
                        Response.Write("</script>");
                    }
                    catch (Exception ex) { Response.Write(ex.Message); }
                }
                else {
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Неверный старый пароль');");
                    Response.Write("</script>");
                }
            }
        }
        
    }
}