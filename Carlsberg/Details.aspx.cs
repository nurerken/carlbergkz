﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Carlsberg_new.DataSet1TableAdapters;
using System.Collections;

namespace Carlsberg_new
{
    public partial class Details : System.Web.UI.Page
    {
        private String p_id = "";
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        string userType = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
           if (User.Identity.IsAuthenticated)
            {
                if (Session["p_id"] != null)
                {
                    string str = sta.GetDataBySID(User.Identity.Name.ToString()).Rows[0]["type"].ToString();
                    if (str.Equals("3"))
                        deleteButton.Visible = true;
                    
                    if (str.Equals("2"))
                        Button2.Visible = true;
                    
                    p_id = Session["p_id"].ToString();
                    Label113.Visible = false;

                    if (!Page.IsPostBack)
                    {
                        DataTable table = dta.GetSingleDataByID(Int32.Parse(p_id));
                        TextBox1.Text = table.Rows[0]["theme"].ToString();
                        TextBox2.Text = table.Rows[0]["description"].ToString();
                        TextBox3.Text = table.Rows[0]["nazvanie"].ToString();
                        TextBox4.Text = table.Rows[0]["workerFIO"].ToString();
                        TextBox5.Text = table.Rows[0]["FIO"].ToString();
                        TextBox6.Text = table.Rows[0]["address"].ToString();
                        TextBox7.Text = table.Rows[0]["codeCRM"].ToString();
                     
                        if (table.Rows[0]["p_type"].ToString().Equals("3"))
                            TextBox8.Text = "закрытая";
                        if (table.Rows[0]["p_type"].ToString().Equals("1") || table.Rows[0]["p_type"].ToString().Equals("2"))
                        {
                            TextBox8.Text = "открытая";
                        }
                        if (table.Rows[0]["p_type"].ToString().Equals("0"))
                            TextBox8.Text = "неназначенная";

                        TextBox10.Text = table.Rows[0]["date"].ToString().Substring(0, 10);
                        TextBox9.Text = table.Rows[0]["comments"].ToString();

                        TextBox111.Text = table.Rows[0]["workerPhone"].ToString();
                        TextBox222.Text = table.Rows[0]["workerEmail"].ToString();
                        TextBox333.Text = table.Rows[0]["phone"].ToString();
                        //TextBox444.Text = table.Rows[0]["email"].ToString();
                        HyperLink1.Text = table.Rows[0]["email"].ToString();
                        HyperLink1.NavigateUrl = "mailto:" + table.Rows[0]["email"].ToString();

                        String photos_before = "";
                        String photos_after = "";

                        photos_before = table.Rows[0]["photo_before"].ToString();
                        photos_after = table.Rows[0]["photo_after"].ToString();

                        String[] photos = photos_before.Split('|');
                        String[] photos2 = photos_after.Split('|');

                        FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photos[0]);
                        if (TheFile.Exists) 
                        {
                            Image1.ImageUrl = "Photos/" + photos[0];
                      /*      Image tempImage = new Image();//original image
                            tempImage.ImageUrl = "Photos/" + photos[0];

                            Image tempImage2 = new Image();//maxsizes
                            tempImage2.Width = 1000;
                            tempImage2.Height = 1000;

                            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
                            else Image1.Width = tempImage2.Width;

                            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
                            else Image1.Height = tempImage2.Height;*/
                        }
                        else Image1.ImageUrl = "null.jpg";

                        TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photos2[0]);
                        if (TheFile.Exists) 
                        {
                            Image2.ImageUrl = "Photos/" + photos2[0];
                           /* Image tempImage = new Image();//original image
                            tempImage.ImageUrl = "Photos/" + photos[0];

                            Image tempImage2 = new Image();//maxsizes
                            tempImage2.Width = 1000;
                            tempImage2.Height = 1000;

                            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image2.Width = tempImage.Width;
                            else Image2.Width = tempImage2.Width;

                            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image2.Height = tempImage.Height;
                            else Image2.Height = tempImage2.Height;*/
                        }
                        else Image2.ImageUrl = "null.jpg";
                            
                        DataList1.DataSource = BindGrid(photos);
                        DataList1.DataBind();

                        DataList2.DataSource = BindGrid(photos2);
                        DataList2.DataBind();
                    }
                }
                else Response.Redirect("default.aspx"); 
            }
            else Response.Redirect("default.aspx"); 
        }

        protected DataTable BindGrid(String[] photos)
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("/Photos/"));
            FileInfo[] fi2 = di.GetFiles();
            List<FileInfo> fi = new List<FileInfo>();

            //Filter images
            for (int i = 0; i < fi2.Count(); i++)
            {
                for (int j = 0; j < photos.Count()-1; j++)
                {
                    if (fi2[i].ToString().Equals(photos[j]))
                    {
                        fi.Add(fi2[i]);
                    }
                }
            }

            // Save all paths to the DataTable as the data source.
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Url", typeof(System.String));
            dt.Columns.Add(dc);

            for (int i = 0; i < fi.Count(); i++)
            {
                DataRow dro = dt.NewRow();
                dro[0] = fi[i].Name;
                dt.Rows.Add(dro);
            }
            return dt;
        }

        protected void deleteSelected(Object sender, EventArgs e)
        {
            try {
                dta.DeleteSingleByID(Int32.Parse(p_id));
                Response.Write("<script type='text/javascript'>");
                Response.Write("alert('заявка удалена');");
                //Response.Write("window.close();");
                Response.Write("document.location.href='moder.aspx';");
                Response.Write("</script>");
            }
            catch (Exception ex) { Response.Write(ex.Message);}
        }

        // Handle the thumbnail image selecting event.676
        protected void imgBtn_Click(object sender, EventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            Image1.ImageUrl = ib.ImageUrl;
            
            Image tempImage = new Image();
            tempImage.ImageUrl = ib.ImageUrl;
            
            Image tempImage2 = new Image();//maxsizes
            tempImage2.Width = 1000;
            tempImage2.Height = 1000;

            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value)Image1.Width = tempImage.Width;                
            else Image1.Width = tempImage2.Width;                
            
            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value)Image1.Height = tempImage.Height;                
            else Image1.Height = tempImage2.Height;                
            
            DataList1.SelectedIndex = Convert.ToInt32(ib.CommandArgument);
        }

        protected void imgBtn_Click2(object sender, EventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            Image2.ImageUrl = ib.ImageUrl;

            Image tempImage = new Image();//original image
            tempImage.ImageUrl = ib.ImageUrl;

            Image tempImage2 = new Image();//maxsizes
            tempImage2.Width = 1000;
            tempImage2.Height = 1000;

            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image2.Width = tempImage.Width;
            else Image2.Width = tempImage2.Width;

            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image2.Height = tempImage.Height;
            else Image2.Height = tempImage2.Height;

            DataList2.SelectedIndex = Convert.ToInt32(ib.CommandArgument);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox7.Text != "")
            {
                G11.DataSource = dta.GetTT(TextBox7.Text);
                G11.DataBind();
            }
        }

        protected void GridView1PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (TextBox7.Text != "")
            {
                G11.PageIndex = e.NewPageIndex;
                G11.DataSource = dta.GetTT(TextBox7.Text);
                G11.DataBind();
            }
        }

        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/details.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            dta.UpdateTypeTo0(Int32.Parse(p_id));
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('статус заявки изменен в неназначенную');");
            Response.Write("document.location.href='admin.aspx';");
            Response.Write("</script>");
        }

    }
}