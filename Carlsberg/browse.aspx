﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="browse.aspx.cs" Inherits="Carlsberg_new.browse" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:loginview ID = "LoginView1" runat="server">
    <AnonymousTemplate>
        <asp:HyperLink runat="server" navigateURL="default.aspx">Вход на сайт</asp:HyperLink>
    </AnonymousTemplate>
    <LoggedInTemplate>
        <table>
            <tr>
            <td style="width:400px"></td>
            <td style="width:400px"></td>
            <td style="width:400px"></td>
                <td style="width:100px">
                    <asp:HyperLink runat="server" navigateURL="EditProfile.aspx">Мои данные</asp:HyperLink>
                </td>
            </tr>
        </table>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
           
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
    Сделать отчет по
        <asp:DropDownList ID = "DropDownList1" runat="server" style="width:100px"></asp:DropDownList>
    заявкам
    <asp:Button ID="Button111" runat="server" Text="Печать" BackColor="White" 
            ForeColor="#0099FF"  OnClick = "printClosed"/>
    
    <br/>
           <table style="height:300px; width:500px">
           <tr>
            <td>
                <asp:Label ID="Label1111" runat="server" Text="От:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label5555" runat="server" Text="До:"></asp:Label>
            </td>
          </tr>

          <tr>
            <td>
                <asp:Label ID="LabelDo" runat="server" Text=""></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelPosle" runat="server" Text=""></asp:Label>
            </td>
          </tr>

          <tr>
            <td>
              <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC" 
        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
        onselectionchanged="Calendar1_SelectionChanged" ShowGridLines="True" 
        Width="220px">
      <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
      <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
      <OtherMonthDayStyle ForeColor="#CC9966" />
      <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
      <SelectorStyle BackColor="#FFCC66" />
      <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
          ForeColor="#FFFFCC" />
      <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
            </td>
            <td>
        <asp:Calendar ID="Calendar2" runat="server" BackColor="#FFFFCC" 
        BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" 
        Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" 
        onselectionchanged="Calendar2_SelectionChanged" ShowGridLines="True" 
        Width="220px">
      <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
      <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
      <OtherMonthDayStyle ForeColor="#CC9966" />
      <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
      <SelectorStyle BackColor="#FFCC66" />
      <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
          ForeColor="#FFFFCC" />
      <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
            </td>
          </tr> 
        </table>     
        </ContentTemplate>      
    </asp:UpdatePanel> 
    <br/>
    <asp:Button ID="Button2" runat="server" Text="Показать заявки" OnClick = "btn1_click"/>
    <br/>
        <asp:GridView ID="GridView3" runat="server" AllowPaging="True" PageSize="30"
                    CellPadding="4" GridLines="None" OnPageIndexChanging = "GridView3PageIndexChanging"
                        AutoGenerateColumns="False" ForeColor="#333333">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>             
                           <asp:TemplateField HeaderText = "&#1058;&#1077;&#1084;&#1072;" >
                                <ItemTemplate>
                                    <asp:LinkButton id="LinkButton2"                                     
                                    Text='<%# Eval("theme") %>'              
                                    OnClick="LinkButton_Click3" 
                                    CommandArgument=<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>
                                    runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <asp:BoundField DataField="workerFIO" HeaderText="ФИО подавшего заявку" 
                                SortExpression="workerFIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="FIO" HeaderText="Ответственный" 
                                SortExpression="FIO" ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="address" HeaderText="&#1040;&#1076;&#1088;&#1077;&#1089;" SortExpression="address" 
                                ItemStyle-Width="100px" >
                <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="date" HeaderText="&#1044;&#1072;&#1090;&#1072;" SortExpression="date" 
                                ItemStyle-Width="80px" >
                <ItemStyle Width="80px"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="p_type" HeaderText="&#1057;&#1090;&#1072;&#1090;&#1091;&#1089;" SortExpression="p_type" 
                                ItemStyle-Width="70px">
                <ItemStyle Width="70px"></ItemStyle>
                            </asp:BoundField>
                        </Columns>

                         <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#5E8444" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <br/>
                    <hr/> 
                    
                    <br/>
                    <asp:Table ID="Table1" runat="server" BackColor="#00FF99">
                    </asp:Table>
    </LoggedInTemplate>   
    </asp:loginview>
</asp:Content>
