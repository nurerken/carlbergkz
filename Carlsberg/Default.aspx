﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Carlsberg_new.Default" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  

<asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
                
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
        .modalPopup
        {
            width: 250px;
            height: 180px;
            background-color: #076DAB;
            color: #FFFFFF;
            border-color: #000000;
            border-width: 1px;
            border-style: solid;
            text-align: center;
            cursor: move;
            font-size: medium;
        }
    .style1
    {
        width: 183px;
    }
        .style2
        {            font-size: medium;
        }
        .style3
        {
            font-size: large;
        }
        .reqFormsubHead
        {
            color: #FFFFFF;
        }
    </style>
    <div>
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none">
        <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Login ID="Login1" runat="server"  DisplayRememberMe="False"
                    TitleText="" UserNameLabelText="Логин:" OnAuthenticate="Login1_Authenticate"
                    FailureText="Неправильный пароль или логин" Width="100%" VisibleWhenLoggedIn="False">
                    <LayoutTemplate>
                        <table border="0" cellpadding="1" cellspacing="0" style="border-collapse: collapse;
                            width: 100%">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Логин:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ctl00$Login1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Пароль:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$Login1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <p>
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Вход" ValidationGroup="ctl00$Login1"
                                            Width="70px" />
                                        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Отмена"
                                            Width="70px" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color: Red;">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    </asp:Login>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>        

    <asp:Panel ID="Panel11" runat="server" BorderWidth="0">
        <table>
            <tr>
                <td style = "width: 3800px">
                <asp:Label ID="Label16" runat="server" Text="Новая заявка:" Font-Bold="True" 
                ForeColor="#0066FF" style="font-size: medium; color: #5E8444"></asp:Label>
                </td>
                <td style = "width: 400px">

        <cc1:ModalPopupExtender ID="LoginStatus1_ModalPopupExtender" runat="server" 
            BackgroundCssClass="modalBackground" PopupControlID="Panel1" 
            TargetControlID="LoginStatus1">
        </cc1:ModalPopupExtender>
                 </td>
            </tr>
        </table>
        <script>
            function clientclick() {
                var mydiv = document.getElementById("div1");
                mydiv.style.visibility = 'visible'; 
                return false;
            }
        </script>
            <div ID="div1" style="border: 2px coral solid; width:270px; position:absolute; top:350px; left:40%; visibility:hidden">
            <asp:login runat="server" BackColor="#F7F6F3" BorderColor="#E6E2D8" 
                    BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
                    Font-Size="0.8em" ForeColor="#333333" Width="260px">
                
                <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                    BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
                <TextBoxStyle Font-Size="0.8em" />
                <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" 
                    ForeColor="White" />        
        </asp:login>    
        </div>
        <br />
        <br/>

        <table class="whitebgBorder" width="100%" border="0" cellpadding="4" cellspacing="0">             
            <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444">
                    <span><span class="style2"><strong>
                    Сведения об инициаторе заявки</strong></span><strong><span class="style3">
                    </span> </strong></span>
                </td>
            </tr>
            <tr>
               <td align="right">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="ФИО:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" Width="489px"></asp:TextBox>
                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1" align="right">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Email:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server" Width="210px"></asp:TextBox>
                    <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1" align="right">
                    <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Телефон:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" ontextchanged="TextBox5_TextChanged" 
                        Width="210px"></asp:TextBox>
                    <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444">
                    <span class="fontBlackBold FontColorStyle1"><span class="style2"><strong>
                    Дата посещения торговой точки:</strong></span><strong><span class="style3">
                    </span> </strong></span>
                </td>
            </tr>             
              </table>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
          <table>
             <tr>                                
                 <td style="width:200px">
                    
                 </td>
                 <td>
                    <asp:TextBox ID="TextBox141" runat="server" Width="200px" ReadOnly="true"></asp:TextBox>                                    
                     <asp:Button ID="Button3" runat="server" Height="22px" onclick="Button3_Click" 
                         Text="..." Width="22px" Visible="False" />
                 </td>                                
              </tr> 

              <tr>
                <td>
                </td>
                <td>
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="White" 
                                        BorderColor="Black" BorderWidth="1px" DayNameFormat="Shortest" 
                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="200px" 
                                        onselectionchanged="Calendar1_SelectionChanged" 
                                        Width="220px" CellPadding="1">
                                        <DayHeaderStyle BackColor="#99CCCC" Height="1px" ForeColor="#336666" />
                                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                        <OtherMonthDayStyle ForeColor="#999999" />
                                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                        <TitleStyle BackColor="#71A88D" Font-Bold="True" Font-Size="10pt" 
                                            ForeColor="White" BorderColor="#3366CC" BorderWidth="1px" Height="25px" />
                                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                        <WeekendDayStyle BackColor="#B4D6BF" />
                                    </asp:Calendar>
                </td>
                
              </tr>
            </table>
            </ContentTemplate>
        </asp:UpdatePanel>     
      <table class="whitebgBorder" width="100%" border="0" cellpadding="4" cellspacing="0"> 
        <tr>
                <td class="reqFormsubHead" colspan="4" nowrap bgcolor="#5E8444">
                    <span class="fontBlackBold FontColorStyle1"><strong><span class="style3">Заявка
                    </span></strong></span>
                </td>
            </tr>
                           <tr>
                                <td class="style1" align="right">
                                    <asp:Label ID="Label7" runat="server" Font-Bold="True" 
                                        Text="Название торговой точки:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox6" runat="server" Width="416px" 
                                        style="margin-top: 0px" ViewStateMode="Disabled"></asp:TextBox>
                                    <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="style1" align="right">
                                    <asp:Label ID="Label3" runat="server" Text="Адрес торговой точки:" 
                                        Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox3" runat="server" Width="479px"></asp:TextBox>
                                    <asp:Label ID="Label11" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               
                                </td>
                                  
                            </tr>
                            <tr>
                                <td class="style1" align="right">
                                    <asp:Label ID="Label2" runat="server" Text="Тема:" 
                                        Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox2" runat="server" Width="479px"></asp:TextBox>
                                    <asp:Label ID="Label12" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1" align="right">
                                    <asp:Label ID="Label8" runat="server" 
                                        Text="Описание:" 
                                        Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox7" runat="server" Height="150px" TextMode="MultiLine" 
                                        Width="478px"></asp:TextBox>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                
                                </td>
                            </tr> 

                            <tr>
            <td class="style1" align="right">
                <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Прикрепить фото"></asp:Label>
                :</td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" class="multi" />
            </td>
        </tr>           
        <tr>
           <td>                   
           <p>
                 &nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:Button ID="Button1" runat="server" 
                            Text="&#1057;&#1086;&#1079;&#1076;&#1072;&#1090;&#1100;" 
                            OnClientClick="return check();"
                            onclick="Button1_Click" Font-Bold="True" Font-Size="Medium" 
                            Font-Strikeout="False" Font-Underline="False"/>
            </p>

        </td>
        </tr>
        <tr>
                <td class="reqFormsubHead" colspan="3" nowrap bgcolor="#5E8444">
                    <span class="fontBlackBold FontColorStyle1"><strong><span class="style3">Просмотр
                    </span></strong></span>
                </td>
            </tr>
        </table>
                                       
        <br />
               
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                Text="Просмотр моих заявок" />
        </p>
       
    </asp:Panel>   
                        
    <script type="text/javascript">
        function test1(str) {
            var numericExpression = /^[0-9]+$/;
            if (str.match(numericExpression)) {
                return true;
            } else {
                //alert(helperMsg);
                //elem.focus();
                return false;
            }
        }

        function validateEmail(text) {  
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(text) == false) {
                return false;
            }
            return true;
        }

        function check() {
            //3,6,2,7
            var t3 = document.getElementById("ContentPlaceHolder1_TextBox3").value;//adres tt
            var t6 = document.getElementById("ContentPlaceHolder1_TextBox6").value;//nazvanie tt
            var t2 = document.getElementById("ContentPlaceHolder1_TextBox2").value;//tema
            var t7 = document.getElementById("ContentPlaceHolder1_TextBox7").value; //opisanie
            var t1 = document.getElementById("ContentPlaceHolder1_TextBox1").value; //opisanie
            var t4 = document.getElementById("ContentPlaceHolder1_TextBox4").value; //opisanie
            var t5 = document.getElementById("ContentPlaceHolder1_TextBox5").value; //opisanie

            if (t6.toString() == "") {
                alert("пожалуйста, заполните поле название торговой точки");
                return false;
            }
            if (t3.toString() == "") {
                alert("пожалуйста, заполните поле адрес торговой точки");
                return false;
            }            
            if (t2.toString() == "") {
                alert("пожалуйста, заполните поле тема");
                return false;
            }
            if (t7.toString() == "") {
                alert("пожалуйста, заполните поле описание");
                return false;
            }
            if (t1.toString() == "") {
                alert("пожалуйста, заполните поле ФИО");
                return false;
            } if (t4.toString() == "") {
                alert("пожалуйста, заполните поле email");
                return false;
            } if (t5.toString() == "") {
                alert("пожалуйста, заполните поле номер телефона");
                return false;
            }
            if (!validateEmail(t4.toString())) {
                alert("пожалуйста, введите правильный почтовый адрес");
                return false;
            }            
            return true;
        }
    </script>
</asp:Content>
