﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="edit2.aspx.cs" Inherits="Carlsberg_new.edit2" %>
<%@ Import Namespace="Carlsberg_new" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:Panel ID="Panel1" runat="server" BorderWidth="1">
    <div>
     <input type="button" value="Назад" onclick="history.back()"><br /> &nbsp;<asp:Label ID="Label11" runat="server" Text="Фото до:"></asp:Label>
                        
                    <table style="height:300px; width:500px">
                        <tr style="height:200px; width:500px">
                            <td>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/null.jpg" style="max-height:400px; max-width:500px;"/>
                            </td>
                        </tr>
                        <tr style="height:100px; width:500px">
                            <td>
                                <asp:DataList ID="DataList1" runat="server" RepeatColumns="5" 
                                    RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgBtn" runat="server" 
                                            CommandArgument="<%# Container.ItemIndex %>" Height="100px" 
                                            ImageUrl='<%# "/Photos/" + Eval("Url") %>' OnClick="imgBtn_Click" 
                                            Width="100px" />
                                    </ItemTemplate>
                                    <SelectedItemStyle BorderColor="Red" BorderWidth="1px" />
                                </asp:DataList>
                            </td>
                        </tr>
                    </table>

    <table style="width: 100%;" border="1">
        <tr>
            <td style="width: 194px">
                <asp:Label ID="Label1" runat="server" Text="&#1058;&#1077;&#1084;&#1072;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>

         <tr>       
            <td style="width: 194px">
                <asp:Label ID="Label2" runat="server" Text="&#1054;&#1087;&#1080;&#1089;&#1072;&#1085;&#1080;&#1077;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Height="172px" 
                    Width="382px" ReadOnly="True" ></asp:TextBox>
            </td>                 
        </tr>      
        
        <tr>       
            <td style="width: 194px">
                <asp:Label ID="Label3" runat="server" Text="&#1053;&#1072;&#1079;&#1074;&#1072;&#1085;&#1080;&#1077; &#1090;&#1086;&#1088;&#1075;&#1086;&#1074;&#1086;&#1081; &#1090;&#1086;&#1095;&#1082;&#1080;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server" Width="255px" ReadOnly="True" ></asp:TextBox>
            </td>                 
        </tr>
        
        <tr>       
            <td style="width: 194px">
                <asp:Label ID="Label4" runat="server" Text="ФИО подавшего заявку:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td>                 
        </tr> 
        
        <tr>       
            <td style="width: 194px">
                <asp:Label ID="Label111" runat="server" Text="Номер телефона подавшего заявку:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox111" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td>                 
        </tr> 

        <tr>       
            <td style="width: 194px">
                <asp:Label ID="Label222" runat="server" Text="Email подавшего заявку:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox222" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td>                 
        </tr> 

         <tr>
            <td style="width: 194px">
                <asp:Label ID="Label5" runat="server" Text="ФИО ответственнего:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox5" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>
         
         <tr>
            <td style="width: 194px">
                <asp:Label ID="Label333" runat="server" Text="Номер телефонa ответственного:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox333" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>

         <tr>
            <td style="width: 194px">
                <asp:Label ID="Label444" runat="server" Text="Email ответственного"></asp:Label>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server">HyperLink</asp:HyperLink>
            </td> 
         </tr>
          
         <tr>
            <td style="width: 194px">
                <asp:Label ID="Label6" runat="server" Text="Адрес TT:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox6" runat="server" Height="24px" Width="255px" 
                    ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>

          <tr>
            <td style="width: 194px">
                <asp:Label ID="Label7" runat="server" Text="&#1050;&#1086;&#1076; &#1057;&#1056;&#1052;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox7" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
                <asp:Button ID="Buttonh" runat="server" onclick="Button11_Click" 
                    Text="Показать историю заявок" />
                <br />
                <asp:GridView ID="G11" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" OnPageIndexChanging="GridView1PageIndexChanging" PageSize="20">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Тема">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" 
                                    CommandArgument="<%# ((DataSet1.DataTable1Row)(((DataRowView)Container.DataItem).Row)).p_id %>" 
                                    OnClick="LinkButton_Click1" Text='<%# Eval("theme") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5E8444" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td> 
         </tr>

          <tr>
            <td style="width: 194px">
                <asp:Label ID="Label8" runat="server" Text="&#1057;&#1090;&#1072;&#1090;&#1091;&#1089;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox8" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>

          <tr>
            <td style="width: 194px">
                <asp:Label ID="Label10" runat="server" Text="&#1044;&#1072;&#1090;&#1072; &#1087;&#1086;&#1076;&#1072;&#1095;&#1080; &#1079;&#1072;&#1103;&#1074;&#1082;&#1080;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox10" runat="server" Width="255px" ReadOnly="True"></asp:TextBox>
            </td> 
         </tr>        
    </table>

    <br />
    <asp:Panel ID="Panel2" runat="server" >
        <div>
            <asp:Label ID="Label12" runat="server" Text="&#1055;&#1088;&#1080;&#1082;&#1088;&#1077;&#1087;&#1080;&#1090;&#1100; &#1092;&#1086;&#1090;&#1086; после" Font-Bold="True"></asp:Label>
            <asp:FileUpload ID="FileUpload1" runat="server" class="multi"/><br/>            
        </div>

        <table>
            <tr>
            <td style="width: 91px" class="input-medium">
                <asp:Label ID="Label9" runat="server" Text="&#1050;&#1086;&#1084;&#1084;&#1077;&#1085;&#1090;&#1072;&#1088;&#1080;&#1081;:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox9" runat="server" Width="380px" TextMode ="MultiLine" 
                    Height="105px"></asp:TextBox>
            </td> 
            </tr>
        </table>

    <asp:Button ID="Button1" runat="server" Text="Закрыть" Font-Bold="True" 
                ForeColor="Black" onclick="Button1_Click" />
    </asp:Panel>
     <input type="button" value="Назад" onclick="history.back()">
    </div>
    </asp:Panel>
    <asp:Label ID="Label113" runat="server" Text="Label"></asp:Label>
</asp:Content>
