﻿using System.Configuration;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Collections;

namespace Carlsberg_new
{
    public partial class edit3 : System.Web.UI.Page
    {
        private String p_id = "";
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        string userType = "";
        string userID = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(userID).Rows[0]["type"].ToString();

                if (userType.Equals("2"))
                {                
                    if (Session["p_id"] != null)
                    {
                        p_id = Session["p_id"].ToString();
                        Label113.Visible = false;

                        if (!Page.IsPostBack)
                        {
                            DataTable table = dta.GetSingleDataByID(Int32.Parse(p_id));

                            TextBox1.Text = table.Rows[0]["theme"].ToString();
                            TextBox2.Text = table.Rows[0]["description"].ToString();
                            TextBox3.Text = table.Rows[0]["nazvanie"].ToString();
                            TextBox4.Text = table.Rows[0]["workerFIO"].ToString();
                            TextBox111.Text = table.Rows[0]["workerPhone"].ToString();
                            TextBox222.Text = table.Rows[0]["workerEmail"].ToString();

                            DropDownList1.DataSource = sta.GetDataForDropDown("0");
                            DropDownList1.DataBind();

                            TextBox6.Text = table.Rows[0]["address"].ToString();
                            //TextBox7.Text = table.Rows[0]["codeCRM"].ToString();
                            TextBox7.Text = "";
                            if (table.Rows[0]["p_type"].ToString().Equals("3"))
                                TextBox8.Text = "закрытая";
                            if (table.Rows[0]["p_type"].ToString().Equals("1") || table.Rows[0]["p_type"].ToString().Equals("2"))
                            {
                                TextBox8.Text = "открытая";
                            }
                            if (table.Rows[0]["p_type"].ToString().Equals("0"))
                                TextBox8.Text = "неназначенная";
                            TextBox10.Text = table.Rows[0]["date"].ToString();
                            TextBox9.Text = table.Rows[0]["comments"].ToString();

                            String photos_before = "";
                            photos_before = table.Rows[0]["photo_before"].ToString();

                            String[] photos = photos_before.Split('|');


                            FileInfo TheFile = new FileInfo(Server.MapPath("Photos") + "\\" + photos[0]);
                            if (TheFile.Exists)
                            {
                                Image1.ImageUrl = "Photos/" + photos[0];
                                Image tempImage = new Image();//original image
                                tempImage.ImageUrl = "Photos/" + photos[0];

                                Image tempImage2 = new Image();//maxsizes
                                tempImage2.Width = 1000;
                                tempImage2.Height = 1000;

                                if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
                                else Image1.Width = tempImage2.Width;

                                if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
                                else Image1.Height = tempImage2.Height;
                            }
                            else Image1.ImageUrl = "null.jpg";
                            
                            DataList1.DataSource = BindGrid(photos);
                            DataList1.DataBind();
                        }
                    }
                    else Response.Redirect("default.aspx");
              }
              else Response.Redirect("default.aspx");
            }
            else Response.Redirect("default.aspx");
        }

        protected DataTable BindGrid(String[] photos)
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("/Photos/"));
            FileInfo[] fi2 = di.GetFiles();
            List<FileInfo> fi = new List<FileInfo>();

            //Filter images
            for (int i = 0; i < fi2.Count(); i++)
            {
                for (int j = 0; j < photos.Count() - 1; j++)
                {
                    if (fi2[i].ToString().Equals(photos[j]))
                    {
                        fi.Add(fi2[i]);
                    }
                }
            }

            // Save all paths to the DataTable as the data source.
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Url", typeof(System.String));
            dt.Columns.Add(dc);

            for (int i = 0; i < fi.Count(); i++)
            {
                DataRow dro = dt.NewRow();
                dro[0] = fi[i].Name;
                dt.Rows.Add(dro);
            }
            return dt;
        }

        // Handle the thumbnail image selecting event.
        protected void imgBtn_Click11(object sender, EventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            Image1.ImageUrl = ib.ImageUrl;

            Image tempImage = new Image();
            tempImage.ImageUrl = ib.ImageUrl;

            Image tempImage2 = new Image();//maxsizes
            tempImage2.Width = 1000;
            tempImage2.Height = 1000;

            if (((Unit)tempImage.Width).Value < ((Unit)tempImage2.Width).Value) Image1.Width = tempImage.Width;
            else Image1.Width = tempImage2.Width;

            if (((Unit)tempImage.Height).Value < ((Unit)tempImage2.Height).Value) Image1.Height = tempImage.Height;
            else Image1.Height = tempImage2.Height;

            DataList1.SelectedIndex = Convert.ToInt32(ib.CommandArgument);
        }
              
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {                
                string supervisor = DropDownList1.SelectedValue;
                string adres = TextBox6.Text;
                string crm = TextBox7.Text;
                string comments = TextBox9.Text;
                string nazvanie = TextBox3.Text;
                 
                if (!supervisor.Equals("0"))
                {
                    dta.UpdateQuery1(supervisor, adres, crm, comments, nazvanie,Int32.Parse(p_id));
                    
                    string sEmail = sta.GetDataBySID(supervisor).Rows[0]["email"].ToString();
                    string wEmail = dta.GetSingleDataByID(Int32.Parse(p_id)).Rows[0]["workerEmail"].ToString();

                    string sPhone = sta.GetDataBySID(User.Identity.Name.ToString()).Rows[0]["phone"].ToString();
                    //send email to supervisor and worker
                    string supervisorName = sta.GetDataBySID(supervisor).Rows[0]["FIO"].ToString();
                    
                    /*sendEmail(sEmail, "Новая заявка", "Здравствуйте," + "\r\n" +
                                                        "в Системе Beer Request зарегистрирована новая заявка с адреса: " + TextBox222.Text + "\r\n" +
                                                        "Тема Заявки: " + TextBox1.Text + "\r\n" +
                                                        "Название торговой точки: " + TextBox3.Text + "\r\n" +
                                                        "ФИО подавшего заявку: " + TextBox4.Text);

                    sendEmail(wEmail, "Назначен супервайзер для вашей заявки в системы Beer Request", "Здравствуйте," + "\r\n" +
                                                        "в Системе Beer Request ваша заявка назначена на супервайзера: " + supervisorName + "\r\n" +
                                                        "Телефон супервайзера " + sPhone  + "\r\n" +
                                                        "E-mail супервайзера " + sEmail + "\r\n" +
                                                        "В ближайшее время супервайзер приступит к ее реализации.");

                    */
                    //-------------- Посыл майло ------------------------------------------------
                    MailAlert Message = new MailAlert();

                    //Супервайзеру
                    MailAlert.AlertList[] Alert = new MailAlert.AlertList[5];
                    Alert[0].ParamName = "@TTName@";    Alert[0].ParamValue = TextBox3.Text;
                    Alert[1].ParamName = "@TTAddress@"; Alert[1].ParamValue = TextBox6.Text;
                    Alert[2].ParamName = "@Subject@";   Alert[2].ParamValue = TextBox1.Text;
                    Alert[3].ParamName = "@Desc@";      Alert[3].ParamValue = TextBox2.Text;
                    Alert[4].ParamName = "@K_Desc@";    Alert[4].ParamValue = TextBox9.Text;
                    Message.SendAlert("new_s", sEmail, "Новая заявка", Alert).ToString();

                    //Инициатору
                    MailAlert.AlertList[] Alert1 = new MailAlert.AlertList[7];
                    Alert1[0].ParamName = "@TTName@";    Alert1[0].ParamValue = TextBox3.Text;
                    Alert1[1].ParamName = "@TTAddress@"; Alert1[1].ParamValue = TextBox6.Text;
                    Alert1[2].ParamName = "@Subject@";   Alert1[2].ParamValue = TextBox1.Text;
                    Alert1[3].ParamName = "@Desc@";      Alert1[3].ParamValue = TextBox2.Text;
                    Alert1[4].ParamName = "@S_FIO@";     Alert1[4].ParamValue = supervisorName;
                    Alert1[5].ParamName = "@S_PhoneNumber@"; Alert1[5].ParamValue = sPhone;
                    Alert1[6].ParamName = "@S_Email@";   Alert1[6].ParamValue = sEmail;
                    Message.SendAlert("assigned", wEmail, "Назначен ответственный по вашей заявке", Alert1).ToString();

                    //---------------------------------------------------------------------------
                    
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Отправлено');");
                    Response.Write("document.location.href='admin.aspx';");
                    //Response.Write("window.close();");
                    Response.Write("</script>");                    
                }
                else {
                    Response.Write("<script type='text/javascript'>");
                    Response.Write("alert('Пожалуйста, выберите супервайзера');");
                    Response.Write("</script>");
                }
                 
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        void sendEmail(string mailto, string subject, string body)
        {
            try
            {
                mydataTableAdapter mta = new mydataTableAdapter();
                string email = mta.GetData().Rows[0]["email"].ToString();
                string password = mta.GetData().Rows[0]["password"].ToString();
                MailMessage objMail = new MailMessage(email, mailto, subject, body);
                NetworkCredential objNC = new NetworkCredential(email, password);
                SmtpClient objsmtp = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTP"), Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort")));
                objsmtp.EnableSsl = true;
                objsmtp.Credentials = objNC;
                objsmtp.Send(objMail);
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBox7.Text != "")
            {
                G11.DataSource = dta.GetTT(TextBox7.Text);
                G11.DataBind();
            }
        }

        protected void GridView1PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (TextBox7.Text != "")
            {
                G11.PageIndex = e.NewPageIndex;
                G11.DataSource = dta.GetTT(TextBox7.Text);
                G11.DataBind();
            }
        }

        protected void LinkButton_Click1(Object sender, EventArgs e)
        {
            string p_id = (sender as LinkButton).CommandArgument;
            Session["p_id"] = p_id;
            Response.Redirect("~/details.aspx");
        }

    }
}