﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carlsberg_new.DataSet1TableAdapters;
using System.Data;

namespace Carlsberg_new
{
    public partial class reports : System.Web.UI.Page
    {
        static DataTable1TableAdapter dta = new DataTable1TableAdapter();
        static supervisorsTableAdapter sta = new supervisorsTableAdapter();
        List<String> supervisors = new List<string>();//working supervisors
        List<String> supervisorNames = new List<string>();//working supervisors
        int open = 0;
        int closed = 0;
        int nenaz = 0;
        string userID = "";
        string userType = "";
        static string date1 = "";
        static string date2 = "";
        string reportType = "0";
        string userFIO = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userID = User.Identity.Name.ToString();
                userType = sta.GetDataBySID(userID).Rows[0]["type"].ToString();

                if (userType.Equals("2") || userType.Equals("4"))
                {
                    if (Request.QueryString["date1"] != null && Request.QueryString["date2"] != null)
                    {
                        date1 = Request.QueryString["date1"];
                        date2 = Request.QueryString["date2"];
                        reportType = Request.QueryString["reporttype"];
                        userFIO = sta.GetDataBySID(userID).Rows[0]["FIO"].ToString();
                        
                        string type = "по всем";
                        if (reportType.Equals("0")) type = "по всем";
                        if (reportType.Equals("1")) type = "по открытым";

                        DataTable dt = sta.GetAllSupervisors("0");
                        foreach (DataRow row in dt.Rows)
                        {
                            supervisors.Add(row["s_id"].ToString());
                            supervisorNames.Add(row["FIO"].ToString());
                        }

                        Label label = new Label();
                        label.Text = "Отчет "+type +" заявкам c " + date1 + " по " + date2;
                        label.Font.Bold = true;
                        this.Page.Form.Controls.Add(label);
                        this.Page.Form.Controls.Add(new LiteralControl("<br/><br/>"));

                        Label label11 = new Label();
                        label11.Text = "неназначенные";
                        this.Page.Form.Controls.Add(label11);

                        GridView GridView11 = new GridView();
                        GridView11.AllowPaging = false;
                        GridView11.CellPadding = 4;
                        GridView11.GridLines = GridLines.None;
                        GridView11.AutoGenerateColumns = false;

                        GridView11.ForeColor = System.Drawing.Color.Black;
                        GridView11.AlternatingRowStyle.BackColor = System.Drawing.Color.White;
                        GridView11.EditRowStyle.BackColor = System.Drawing.Color.FromName("#2461BF");
                        GridView11.FooterStyle.BackColor = System.Drawing.Color.FromName("#507CD1");
                        GridView11.FooterStyle.ForeColor = System.Drawing.Color.White;
                        GridView11.FooterStyle.Font.Bold = true;
                        GridView11.HeaderStyle.BackColor = System.Drawing.Color.FromName("#507CD1");
                        GridView11.HeaderStyle.ForeColor = System.Drawing.Color.White;
                        GridView11.HeaderStyle.Font.Bold = true;
                        GridView11.PagerStyle.BackColor = System.Drawing.Color.FromName("#2461BF");
                        GridView11.PagerStyle.ForeColor = System.Drawing.Color.White;
                        GridView11.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
                        GridView11.RowStyle.BackColor = System.Drawing.Color.FromName("#EFF3FB");
                        GridView11.SelectedRowStyle.BackColor = System.Drawing.Color.FromName("#D1DDF1");
                        GridView11.SelectedRowStyle.Font.Bold = true;
                        GridView11.SelectedRowStyle.ForeColor = System.Drawing.Color.FromName("#333333");
                        GridView11.DataSource = dta.GetNenaznachennyhZaiavokByDate(Convert.ToDateTime(date1), Convert.ToDateTime(date2));

                        BoundField bf11 = new BoundField();
                        bf11.DataField = "theme";
                        bf11.HeaderText = "тема";
                        bf11.ItemStyle.Width = 70;
                        GridView11.Columns.Add((DataControlField)bf11);

                        BoundField bf21 = new BoundField();
                        bf21.DataField = "workerFIO";
                        bf21.HeaderText = "ФИО подавшего заявку";
                        bf21.ItemStyle.Width = 100;
                        GridView11.Columns.Add((DataControlField)bf21);

                        BoundField bf31 = new BoundField();
                        bf31.DataField = "nazvanie";
                        bf31.HeaderText = "Название ТТ";
                        bf31.ItemStyle.Width = 100;
                        GridView11.Columns.Add((DataControlField)bf31);

                        BoundField bf41 = new BoundField();
                        bf41.DataField = "address";
                        bf41.HeaderText = "адрес ТТ";
                        bf41.ItemStyle.Width = 100;
                        GridView11.Columns.Add((DataControlField)bf41);

                        BoundField bf51 = new BoundField();
                        bf51.DataField = "date";
                        bf51.HeaderText = "Дата подачи заявки";
                        bf51.ItemStyle.Width = 80;
                        GridView11.Columns.Add((DataControlField)bf51);

                        BoundField bf61 = new BoundField();
                        bf61.DataField = "p_type";
                        bf61.HeaderText = "Статус";
                        bf61.ItemStyle.Width = 70;
                        GridView11.Columns.Add((DataControlField)bf61);

                        GridView11.DataBind();
                        this.Page.Form.Controls.Add(GridView11);

                        int rows1 = GridView11.Rows.Count;
                        open = 0;
                        closed = 0;
                        nenaz = 0;

                        for (int i = 0; i < rows1; i++)
                        {
                            if (GridView11.Rows[i].Cells[5].Text == "3")
                            {
                                GridView11.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                                closed++;
                            }
                            if (GridView11.Rows[i].Cells[5].Text.Equals("1") || GridView11.Rows[i].Cells[5].Text == "2")
                            {
                                GridView11.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                                open++;
                            }
                            if (GridView11.Rows[i].Cells[5].Text == "0") { 
                                GridView11.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                                nenaz++;
                            }

                            GridView11.Rows[i].Cells[4].Text = (GridView11.Rows[i].Cells[4].Text).Substring(0, 10);
                        }

                        Label label21 = new Label();
                        label21.Text = " <b>всего:</b> " + nenaz;
                        this.Page.Form.Controls.Add(label21);

                        this.Page.Form.Controls.Add(new LiteralControl("<hr/>"));
                        
                        for (int j = 0; j < supervisors.Count(); j++)
                        {
                            Label label1 = new Label();
                            label1.Text = "супервайзор: " + supervisorNames[j];
                            //this.Page.Form.Controls.Add(label1);

                            GridView GridView1 = new GridView();
                            GridView1.AllowPaging = false;
                            GridView1.CellPadding = 4;
                            GridView1.GridLines = GridLines.None;
                            GridView1.AutoGenerateColumns = false;

                            GridView1.ForeColor = System.Drawing.Color.Black;
                            GridView1.AlternatingRowStyle.BackColor = System.Drawing.Color.White;
                            GridView1.EditRowStyle.BackColor = System.Drawing.Color.FromName("#2461BF");
                            GridView1.FooterStyle.BackColor = System.Drawing.Color.FromName("#507CD1");
                            GridView1.FooterStyle.ForeColor = System.Drawing.Color.White;
                            GridView1.FooterStyle.Font.Bold = true;
                            GridView1.HeaderStyle.BackColor = System.Drawing.Color.FromName("#507CD1");
                            GridView1.HeaderStyle.ForeColor = System.Drawing.Color.White;
                            GridView1.HeaderStyle.Font.Bold = true;
                            GridView1.PagerStyle.BackColor = System.Drawing.Color.FromName("#2461BF");
                            GridView1.PagerStyle.ForeColor = System.Drawing.Color.White;
                            GridView1.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
                            GridView1.RowStyle.BackColor = System.Drawing.Color.FromName("#EFF3FB");
                            GridView1.SelectedRowStyle.BackColor = System.Drawing.Color.FromName("#D1DDF1");
                            GridView1.SelectedRowStyle.Font.Bold = true;
                            GridView1.SelectedRowStyle.ForeColor = System.Drawing.Color.FromName("#333333");
                            if(reportType.Equals("0")) GridView1.DataSource = dta.GetDataBySupervisorForReport(supervisors[j], Convert.ToDateTime(date1), Convert.ToDateTime(date2));
                            if (reportType.Equals("1")) GridView1.DataSource = dta.GetDataBySupervisorForReportOpen(supervisors[j], Convert.ToDateTime(date1), Convert.ToDateTime(date2));

                            BoundField bf = new BoundField();
                            bf.DataField = "theme";
                            bf.HeaderText = "тема";
                            bf.ItemStyle.Width = 70;
                            GridView1.Columns.Add((DataControlField)bf);

                            BoundField bf2 = new BoundField();
                            bf2.DataField = "workerFIO";
                            bf2.HeaderText = "ФИО подавшего заявку";
                            bf2.ItemStyle.Width = 100;
                            GridView1.Columns.Add((DataControlField)bf2);

                            BoundField bf3 = new BoundField();
                            bf3.DataField = "nazvanie";
                            bf3.HeaderText = "Название ТТ";
                            bf3.ItemStyle.Width = 100;
                            GridView1.Columns.Add((DataControlField)bf3);

                            BoundField bf4 = new BoundField();
                            bf4.DataField = "address";
                            bf4.HeaderText = "адрес ТТ";
                            bf4.ItemStyle.Width = 100;
                            GridView1.Columns.Add((DataControlField)bf4);

                            BoundField bf5 = new BoundField();
                            bf5.DataField = "date";
                            bf5.HeaderText = "Дата подачи заявки";
                            bf5.ItemStyle.Width = 80;
                            GridView1.Columns.Add((DataControlField)bf5);

                            BoundField bf6 = new BoundField();
                            bf6.DataField = "p_type";
                            bf6.HeaderText = "Статус";
                            bf6.ItemStyle.Width = 70;
                            GridView1.Columns.Add((DataControlField)bf6);

                            GridView1.DataBind();
                            //this.Page.Form.Controls.Add(GridView1);

                            int rows = GridView1.Rows.Count;
                            open = 0;
                            closed = 0;
                            nenaz = 0;

                            if (rows != 0)
                            {
                                this.Page.Form.Controls.Add(label1);
                                this.Page.Form.Controls.Add(GridView1);
                            }

                            if (reportType.Equals("0"))
                            {
                                for (int i = 0; i < rows; i++)
                                {
                                    if (GridView1.Rows[i].Cells[5].Text == "3")
                                    {
                                        GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                                        closed++;
                                    }
                                    if (GridView1.Rows[i].Cells[5].Text.Equals("1") || GridView1.Rows[i].Cells[5].Text == "2")
                                    {
                                        GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                                        open++;
                                    }
                                    if (GridView1.Rows[i].Cells[5].Text == "0") GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";

                                    GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
                                }

                                Label label2 = new Label();
                                label2.Text = "<b>закрытых:</b> " + closed + ", <b>открытых:</b> " + open;
                                if(rows!=0)
                                    this.Page.Form.Controls.Add(label2);
                            }
                            if (reportType.Equals("1")) {//open
                                for (int i = 0; i < rows; i++)
                                {
                                    if (GridView1.Rows[i].Cells[5].Text == "3")
                                    {
                                        GridView1.Rows[i].Cells[5].Text = "<b style=color:blue>Закрытая</b>";
                                        closed++;
                                    }
                                    if (GridView1.Rows[i].Cells[5].Text.Equals("1") || GridView1.Rows[i].Cells[5].Text == "2")
                                    {
                                        GridView1.Rows[i].Cells[5].Text = "<b style=color:green>Открытая</b>";
                                        open++;
                                    }
                                    if (GridView1.Rows[i].Cells[5].Text == "0")
                                    {
                                        GridView1.Rows[i].Cells[5].Text = "<b style=color:red>Неназначенная</b>";
                                        nenaz++;
                                    }

                                    GridView1.Rows[i].Cells[4].Text = (GridView1.Rows[i].Cells[4].Text).Substring(0, 10);
                                }

                                Label label2 = new Label();
                                label2.Text = "<b>всего:</b> " + open;
                                if(rows!=0)
                                    this.Page.Form.Controls.Add(label2);
                            }
                            if (rows != 0)
                                this.Page.Form.Controls.Add(new LiteralControl("<hr/>"));
                        }
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Print", "javascript:window.print();", true);
                    
                    }
                    else { Response.Redirect("default.aspx"); }
                }
                else { Response.Redirect("default.aspx"); }
            }
            else { Response.Redirect("default.aspx"); }
        }
    }
}